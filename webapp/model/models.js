sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			Device.isNotPhone = !Device.system.phone;
			Device.isPhone = Device.system.phone;
			Device.isHybridApp = this.getIsHybridApp();
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createSyncModel: function() {
			var oModel = new JSONModel({
				SyncColor: "",
				SyncIcon: "",
				LastSyncTime: "",
				Online: false,
				PendingLocalData: false,
				IsSynching: false,
				InErrorState: false,
				Errors: [],
				OrderErrors: [],
				NoticationErrors: [],
				ErrorListContextObject: "", //The object to limit the errors shown in the error list. For instance Order
				ErrorListContextID: "", // The object ID to limit the errors shown in the error list. For instance Order ID
				WentOfflineTime : null
			});

			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},

		createAppInfoModel: function() {
			var oModel = new JSONModel({
				AppVersion: "AppVersion",
				AppName: "AppName",
				WebSite: "http://www.mobileworkorder.info/",
				UserName: "",
				UserFirstName: "",
				UserFullName: "",
				UserPosition: "",
				UserImage: "",
				Persno: "",
				UILanguage: sap.ui.getCore().getConfiguration().getLanguage(),
				UILanguageText: "",
				HasNoImage : false
			});

			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createSelectObjectForNewNotificationModel: function() {
			var oModel = new JSONModel({
				equipmentNo: "",
				equipmentDesc: "",
				functionalLoc: "",
				funcLocDesc: ""
			});

			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},

		createTimeRegistrationTimerModel: function() {
			var oModel = new JSONModel({
				Started: false,
				OrderId: "",
				OrderIdNumber: "",
				OrderShortText: "",
				StartDateTime: ""
			});

			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},

		createLanguagesModel: function() {
			var oModel = new JSONModel({
				Languages: [{
					LanguageCode: ["cs-CZ", "cs-cz"],
					LanguageText: "Český",
					Image: "/images/flags/cz.png"
				}, {
					LanguageCode: ["zh-CN", "zh-cn"],
					LanguageText: "简体中文",
					Image: "/images/flags/zh.png"
				}, {
					LanguageCode: ["da-DK", "da-dk"],
					LanguageText: "Dansk",
					Image: "/images/flags/da.png"
				}, {
				// 	LanguageCode: ["de-DE", "de-de"],
				// 	LanguageText: "Deutsch",
				// 	Image: "/images/flags/de.png"
				// }, {
					LanguageCode: ["en-GB", "en-gb"],
					LanguageText: "English (UK)",
					Image: "/images/flags/uk.png"
				}, {
				// 	LanguageCode: ["en-US", "en-us"],
				// 	LanguageText: "English (US)",
				// 	Image: "/images/flags/us.png"
				// }, {
				// 	LanguageCode: ["es-ES", "es-es"],
				// 	LanguageText: "España (Spain)",
				// 	Image: "/images/flags/es.png"
				// }, {
					LanguageCode: ["es-MX", "es-mx"],
					LanguageText: "Español (México)",
					Image: "/images/flags/mx.png"
				}, {
					LanguageCode: ["hu-HU", "hu-hu"],
					LanguageText: "Magyar",
					Image: "/images/flags/hu.png"
				// }, {
				// 	LanguageCode: ["nb-NO", "nb-no"],
				// 	LanguageText: "Norsk bokmål",
				// 	Image: "/images/flags/no.png"
				// }, {
				// 	LanguageCode: ["sv-SE", "sv-se"],
				// 	LanguageText: "Svenska",
				// 	Image: "/images/flags/se.png"
				}
				]
			});

			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		getIsHybridApp: function() {
			if (sap.hybrid) {
				return true;
			}
			return false;
		}
	};
});