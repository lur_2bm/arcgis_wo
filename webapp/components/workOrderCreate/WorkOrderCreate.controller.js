sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox",
	"sap/ui/model/Filter"
], function(Controller, History, MessageBox, Filter) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.workOrderCreate.WorkOrderCreate", {
		onInit: function() {
			this.getRouter().getRoute("workOrderCreate").attachMatched(this.onRouteMatched, this);

			sap.m.DatePicker.prototype.onAfterRendering = function(e) {
				$('#' + e.srcControl.getId() + " -inner").prop('readonly', true);
			};
		},

		clearViewModel: function(notificationId) {
			if (notificationId) {
				this.getView().getModel("viewModel").getData().CreateFromNotification = true;
				this.getView().getModel("viewModel").getData().CreateWithoutNotification = false;
				this.getView().getModel("viewModel").getData().NotificationId = notificationId;
			} else {
				this.getView().getModel("viewModel").getData().CreateFromNotification = false;
				this.getView().getModel("viewModel").getData().CreateWithoutNotification = true;
				this.getView().getModel("viewModel").getData().NotificationId = "";
			}
			this.getView().getModel("viewModel").refresh();
		},

		onRouteMatched: function(oEvent) {
			var direction = sap.ui.core.routing.History.getInstance().getDirection();

			if (direction !== "Backwards") {

				this.getView().setModel(new sap.ui.model.json.JSONModel({
					WorkCenterSelectedText: "",
					WorkCenterSelectedID: "",
					OrderTypeSelectedType: "",
					OrderTypeSelectedText: ""
				}), "viewModel");

				var oArguments = oEvent.getParameter("arguments");
				var notificationId = oArguments.notificationId;

				if (notificationId !== "NONOTIFICATIONID") {
					//If notificationId is specified create order from notification
					this.clearViewModel(notificationId);
				} else {
					this.clearViewModel("");
				}

				this.newEntry = this.getView().getModel().createEntry("/OrderSet", {
					success: function(oData, oResponse) {
						this.getView().setBusy(false);
						this.getRouter().navTo("workOrderDetails", {
							workOrderContext: this.newEntry.getPath().substr(1)
						}, true);

					}.bind(this),
					error: function(error) {
						if (error.statusCode === 0) {
							return;
						}

						this.getView().setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				});
			}

			var selectObjectForNewWorkOrderModel = this.getView().getModel("selectObjectForNewNotificationModel");

			if (!jQuery.isEmptyObject(selectObjectForNewWorkOrderModel.getData())) {
				if (this.newEntry) {
					if (selectObjectForNewWorkOrderModel.getData().equipmentNo !== "NONE" && selectObjectForNewWorkOrderModel.getData().equipmentDesc !==
						"NONE") {
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", selectObjectForNewWorkOrderModel.getData().equipmentNo);
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equidescr", selectObjectForNewWorkOrderModel.getData().equipmentDesc);
					}

					this.getView().getModel().setProperty(this.newEntry.getPath() + "/Funcldescr", selectObjectForNewWorkOrderModel.getData().funcLocDesc);
					this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctLoc", selectObjectForNewWorkOrderModel.getData().functionalLoc);
				}
			}

			this.getView().setBindingContext(this.newEntry);
		},

		clearCreateWorkOrderModel: function() {
			//Reset create work order model
			var selectObjectForNewWorkOrderModel = this.getView().getModel("selectObjectForNewNotificationModel");
			selectObjectForNewWorkOrderModel.setData({});
		},

		onNavigateBack: function(oEvent) {
			this.clearCreateWorkOrderModel();

			//Delete created entry
			this.getView().getModel().deleteCreatedEntry(this.newEntry);

			//reset any error state
			this.getView().byId("workOrderShortDesc").setValueState("None");
			this.getView().byId("workOrderType").setValueState("None");
			this.getView().byId("workCenterInput").setValueState("None");

			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			this.getView().setBindingContext(null);

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("dashboard", true);
			}
		},

		onSaveWorkOrder: function() {
			if (!this.notificationInformationValidates()) {
				sap.m.MessageToast.show(this.getI18nText("NotificationCreate-SaveValidation-Failed"));
				return;
			}

			// Check for empty dates
			if (this.getView().getBindingContext().getObject().StartDate === undefined) {
				this.getView().getModel().setProperty("StartDate", new Date(),
					this.getView().getBindingContext());
			}

			if (this.getView().getBindingContext().getObject().FinishDate === undefined) {
				this.getView().getModel().setProperty("FinishDate", new Date(),
					this.getView().getBindingContext());
			}

			// Add value for order type and work center 
			this.getView().getModel().setProperty("OrderType", this.getView().getModel("viewModel").getData().OrderTypeSelectedType,
				this.getView().getBindingContext());
			this.getView().getModel().setProperty("NotifNo", this.getView().getModel("viewModel").getData().NotificationId,
				this.getView().getBindingContext());
			this.getView().getModel().setProperty("OrderTypeTxt", this.getView().getModel("viewModel").getData().OrderTypeSelectedText,
				this.getView().getBindingContext());
			this.getView().getModel().setProperty("MnWkctrId", this.getView().getModel("viewModel").getData().WorkCenterSelectedID, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("MnWkCtr", this.getView().getModel("viewModel").getData().WorkCenterSelectedType, this.getView()
				.getBindingContext());
			//These attributes needed to make order look complete in offline mode also
			this.getView().getModel().setProperty("Assignedtome", "X", this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("AssignedBy", this.getView().getModel("appInfoModel").getData().UserFullName, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("AssignedTo", this.getView().getModel("appInfoModel").getData().UserFullName, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("Personresp", this.getView().getModel("appInfoModel").getData().Persno, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("OrderStatus", "INITIAL", this.getView()
				.getBindingContext());

			this.getView().setBusy(true);
			this.getView().getModel().submitChanges({
				success: jQuery.proxy(function(oData, oResponse) {
					this.getView().setBusy(false);
					this.clearCreateWorkOrderModel();
				}, this),
				error: jQuery.proxy(function(error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}, this)
			});
		},

		notificationInformationValidates: function() {

			var validationError = false;

			//reset
			this.getView().byId("workOrderShortDesc").setValueState("None");
			this.getView().byId("workOrderType").setValueState("None");
			this.getView().byId("workCenterInput").setValueState("None");

			if (!this.getView().getBindingContext().getObject().ShortText) {
				this.getView().byId("workOrderShortDesc").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("workOrderType").getValue()) {
				this.getView().byId("workOrderType").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("workCenterInput").getValue()) {
				this.getView().byId("workCenterInput").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().getModel("viewModel").getData().CreateFromNotification && !this.getView().getBindingContext().getObject().FunctLoc) {
				//this.getView().byId("funcLoc").setValueState("Error");
				validationError = true;
			}

			if (validationError) {
				return false;
			}

			return true;
		},

		onSelectBtnPress: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			oRouter.navTo("structureBrowser", {
				notificationContext: this.newEntry.getPath().substr(1),
				parentView: "notificationCreate"
			}, false);
		},

		onScanBtnPress: function() {
			var isHybridApp = this.getView().getModel("device").getData().isHybridApp;
			if (isHybridApp) {
				cordova.plugins.barcodeScanner.scan(
					function(result) {
						if (result.cancelled) {
							return;
						} else {
							var scannedEquipmentId = result.text;
							var onDataReceived = {
								success: function(oData, oResponse) {
									var isFuncLoc = false;
									this.saveEquipmentToModel(oData, isFuncLoc);

								}.bind(this),
								error: function(oData, oResponse) {
									if (oData.statusCode === "404") {
										this.searchFuncLocWithId(scannedEquipmentId);
									} else {
										this.errorCallBackShowInPopUp();
									}
								}.bind(this)
							};
							this.getView().getModel().read("/EquipmentsSet('" + scannedEquipmentId + "')", onDataReceived);
						}
					}.bind(this),
					function() {
						sap.m.MessageToast.show(this.getI18nText("MaterialScanningFailed"));
					}.bind(this), {
						showTorchButton: true, // iOS and Android
						torchOn: true, // Android, launch with the torch switched on (if available)
						orientation: "", // Android only (portrait|landscape), default unset so it rotates with the device
						formats: "ITF,CODE_128,EAN_8,EAN_13", // default: all but PDF_417 and RSS_EXPANDED
						disableAnimations: true, // iOS
						disableSuccessBeep: true // iOS
					}
				);
			} else {
				this.showAlertNotDevice();
			}
		},

		saveEquipmentToModel: function(equipmentObject, isFuncLoc) {

			if (isFuncLoc) {
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctLoc", equipmentObject.FunctionalLocation);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/Funcldescr", equipmentObject.Description);

			} else {
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctLoc", equipmentObject.Funcloc);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/Funcldescr", equipmentObject.Funclocdesc);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", equipmentObject.Equipment);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equidescr", equipmentObject.Description);
			}
		},

		searchFuncLocWithId: function(equipId) {
			var onDataReceived = {
				success: function(oData, oResponse) {
					var isFuncLoc = true;
					this.saveEquipmentToModel(oData, isFuncLoc);
				}.bind(this),
				error: function(oData, oResponse) {
					if (oData.statusCode === "404") {
						this.showAlertNoObjectFound(equipId);

					} else {
						this.errorCallBackShowInPopUp();
					}
				}.bind(this)
			};

			this.getView().getModel().read("/FunctionalLocationsSet('" + equipId + "')", onDataReceived);
		},

		showAlertNoObjectFound: function(equipmentNo) {
			// Reset values if object was not found
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctLoc", "");
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/Funcldescr", "");
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", "");
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equidescr", "");

			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.show(this.getI18nTextReplace1("Dashboard-scanObjectTile-searchObjectNotFoundMsgText", equipmentNo), {
				icon: MessageBox.Icon.NONE,
				title: this.getI18nText("Dashboard-scanObjectTile-searchObjectNotFoundMsgTitleText"),
				actions: [MessageBox.Action.OK],
				defaultAction: MessageBox.Action.OK,
				styleClass: bCompact ? "sapUiSizeCompact" : ""
			});
		},

		showAlertNotDevice: function() {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.show(this.getI18nText("Dashboard-scanObjectTile-isNotDeviceMsgText"), {
				icon: MessageBox.Icon.NONE,
				title: this.getI18nText("Dashboard-scanObjectTile-isNotDeviceMsgTitleText"),
				actions: [MessageBox.Action.OK],
				defaultAction: MessageBox.Action.OK,
				styleClass: bCompact ? "sapUiSizeCompact" : ""
			});
		},

		onFilterWorkCenterSelect: function(oEvent) {
			if (!this._workCenterPopover) {
				this._workCenterPopover = sap.ui.xmlfragment("WorkCenterPopover",
					"com.twobm.mobileworkorder.components.notificationCreate.fragments.WorkCenterFilterSelect",
					this);
				this._workCenterPopover.getAggregation('_dialog').getSubHeader().setVisible(false);
				// this._workCenterPopover.getAggregation('_dialog').getSubHeader().getContentMiddle()[0].setPlaceholder(this.getI18nText(
				// 	"NotificationCreate-WorkCenterFilterSelect-SearchField-Placeholder"));
				this._workCenterPopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
				this._workCenterPopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
				this._workCenterPopover.getAggregation('_dialog').setVerticalScrolling(true);

				//this.setupWorkCenterSelectTemplate();

				this.getView().addDependent(this._workCenterPopover);
			}

			// this._workCenterPopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
			// 	path: "/WorkCenterSet",
			// 	template: this.workCenterSelectTemplate,
			// 	sorter: {
			// 		path: 'Ktext',
			// 		descending: false
			// 	}
			// });

			this._workCenterPopover.open();
		},

		onFilterWorkCenterSelected: function(oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().WorkCenterSelectedType = selectedItem.getTitle(); //.data("WORKCENTER");
			viewModel.getData().WorkCenterSelectedText = selectedItem.getDescription(); //.data("KTEXT");
			viewModel.getData().WorkCenterSelectedID = selectedItem.data("ID");
			viewModel.refresh();
		},

		searchWorkCenterPress: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchWorkCenter(searchString, itemsBinding);
		},

		searchWorkCenterLive: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchWorkCenter(searchString, itemsBinding);
		},

		searchWorkCenter: function(sValue, itemsBinding) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();

			aFilters.push(new sap.ui.model.Filter("Ktext", sap.ui.model.FilterOperator.Contains, searchString));
			aFilters.push(new sap.ui.model.Filter("Ktext", sap.ui.model.FilterOperator.StartsWith, searchString));

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		formatSelectedWorkCenter: function(type, description) {
			if (type) {
				return type + " - " + description;
			}

			return "";
		},

		onFilterOrderTypeSelect: function(oEvent) {
			if (!this._orderTypePopover) {
				this._orderTypePopover = sap.ui.xmlfragment("OrderTypePopover",
					"com.twobm.mobileworkorder.components.workOrderCreate.fragments.OrderTypeFilterSelect",
					this);
				this._orderTypePopover.getAggregation('_dialog').getSubHeader().setVisible(false);
				// this._notificationTypePopover.getAggregation('_dialog').getSubHeader().getContentMiddle()[0].setPlaceholder(this.getI18nText(
				// 	"NotificationCreate-NotificationTypeFilterSelect-SearchField-Placeholder"));
				this._orderTypePopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
				this._orderTypePopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
				this._orderTypePopover.getAggregation('_dialog').setVerticalScrolling(true);

				//this.setupOrderTypeSelectTemplate();

				this.getView().addDependent(this._orderTypePopover);
			}

			// this._orderTypePopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
			// 	path: "/OrderTypesSet",
			// 	template: this.orderTypeSelectTemplate,
			// 	sorter: {
			// 		path: "MessageTypeTxt",
			// 		descending: false
			// 	}
			// });

			this._orderTypePopover.open();
		},

		// setupOrderTypeSelectTemplate: function() {
		// 	this.orderTypeSelectTemplate = new sap.m.StandardListItem({
		// 		title: "{OrderType}",
		// 		description: "{OrderTypeTxt}",
		// 		active: true
		// 	});

		// 	this.orderTypeSelectTemplate.data("TYPE", "{OrderType}");
		// 	this.orderTypeSelectTemplate.data("TEXT", "{OrderTypeTxt}");
		// },

		onFilterOrderTypeSelected: function(oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().OrderTypeSelectedType = selectedItem.getTitle(); //.data("TYPE");
			viewModel.getData().OrderTypeSelectedText = selectedItem.getDescription(); //data("TEXT");
			viewModel.refresh();

			this.getView().byId("workOrderType").setValueState("None");
		},

		searchOrderTypePress: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchOrderType(searchString, itemsBinding);
		},

		searchOrderTypeLive: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchOrderType(searchString, itemsBinding);
		},

		searchOrderType: function(sValue, itemsBinding) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();

			aFilters.push(new sap.ui.model.Filter("MessageTypeTxt", sap.ui.model.FilterOperator.Contains, searchString));
			aFilters.push(new sap.ui.model.Filter("MessageTypeTxt", sap.ui.model.FilterOperator.StartsWith, searchString));

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		formatOrderTypeIsSelected: function(type) {
			if (type) {
				return true;
			}
			return false;
		},

		onShortDescriptionChanged: function() {
			//Reset any error state
			this.getView().byId("shortDescription").setValueState("None");
		}
	});
});