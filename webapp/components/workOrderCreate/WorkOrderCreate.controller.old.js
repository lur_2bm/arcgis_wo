sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox",
	"sap/m/MessageToast"
], function(Controller, History, MessageBox, MessageToast) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.workOrderCreate.WorkOrderCreate", {
		onInit: function() {
			this.getRouter().getRoute("workOrderCreate").attachMatched(this.onRouteMatched, this);

			this.getView().setModel(new sap.ui.model.json.JSONModel(), "viewModel");
			this.clearViewModel("");
		},

		clearViewModel: function(notificationId) {
			if (notificationId) {
				this.getView().getModel("viewModel").getData().CreateFromNotification = true;
				this.getView().getModel("viewModel").getData().CreateWithoutNotification = false;
				this.getView().getModel("viewModel").getData().NotificationId = notificationId;
			} else {
				this.getView().getModel("viewModel").getData().CreateFromNotification = false;
				this.getView().getModel("viewModel").getData().CreateWithoutNotification = true;
				this.getView().getModel("viewModel").getData().NotificationId = "";
			}
			this.getView().getModel("viewModel").refresh();
		},

		onRouteMatched: function(oEvent) {
			var oArguments = oEvent.getParameter("arguments");
			var notificationId = oArguments.notificationId;

			if (notificationId !== "NONOTIFICATIONID") {
				//If notificationId is specified create order from notification
				this.clearViewModel(notificationId);
			} else {
				this.clearViewModel("");
			}

			var direction = sap.ui.core.routing.History.getInstance().getDirection();

			if (direction !== "Backwards") {
				var _date = new Date();
				_date.setDate(_date.getDate());
				//Clear data
				var data = {
					shortText: "",
					NotifNo: notificationId,
					OrderType: "",
					MnWkCtr: "",
					StartDate: _date,
					FinishDate: _date,
					FunctLoc: "",
					FuncLocDesc: "",
					Equipment: "",
					EquipmentDesc: ""

				};
				var ordModel = new sap.ui.model.json.JSONModel(data);
				this.getView().setModel(ordModel, "ordModel");
			}

			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");
			
			if (!jQuery.isEmptyObject(selectObjectForNewNotificationModel.getData())) {

				if (selectObjectForNewNotificationModel.getData().equipmentNo !== "NONE" && selectObjectForNewNotificationModel.getData().equipmentDesc !==
					"NONE") {
					this.getView().getModel("ordModel").setProperty("/Equipment", selectObjectForNewNotificationModel.getData().equipmentNo);
					this.getView().getModel("ordModel").setProperty("/EquipmentDesc", selectObjectForNewNotificationModel.getData()
						.equipmentDesc);
				}

				this.getView().getModel("ordModel").setProperty("/FuncLocDesc", selectObjectForNewNotificationModel.getData().funcLocDesc);
				this.getView().getModel("ordModel").setProperty("/FunctLoc", selectObjectForNewNotificationModel.getData().functionalLoc);
			}

		},
		onFilterOrderTypeSelect: function(oEvent) {
			if (!this._orderTypePopover) {
				this._orderTypePopover = sap.ui.xmlfragment("OrderTypePopover",
					"com.twobm.mobileworkorder.components.workOrderCreate.fragments.OrderTypeFilterSelect",
					this);
				this._orderTypePopover.getAggregation('_dialog').getSubHeader().setVisible(false);
				// this._notificationTypePopover.getAggregation('_dialog').getSubHeader().getContentMiddle()[0].setPlaceholder(this.getI18nText(
				// 	"NotificationCreate-NotificationTypeFilterSelect-SearchField-Placeholder"));
				this._orderTypePopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
				this._orderTypePopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
				this._orderTypePopover.getAggregation('_dialog').setVerticalScrolling(true);

				this.setupOrderTypeSelectTemplate();

				this.getView().addDependent(this._orderTypePopover);
			}
			this._orderTypePopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
				path: "/OrderTypesSet",
				template: this.orderTypeSelectTemplate,
				sorter: {
					path: "OrderTypeTxt",
					descending: false
				}
			});

			this._orderTypePopover.open();
		},
		setupOrderTypeSelectTemplate: function() {
			this.orderTypeSelectTemplate = new sap.m.StandardListItem({
				title: "{OrderType}",
				description: "{OrderTypeTxt}",
				active: true
			});

			this.orderTypeSelectTemplate.data("TYPE", "{OrderType}");
			this.orderTypeSelectTemplate.data("TEXT", "{OrderTypeTxt}");
		},

		onFilterOrderTypeSelected: function(oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var ordModel = this.getView().getModel("ordModel");
			ordModel.getData().OrderType = selectedItem.data("TYPE");
			ordModel.refresh();
		},
		onFilterWorkCenterSelect: function(oEvent) {
			if (!this._workCenterPopover) {
				this._workCenterPopover = sap.ui.xmlfragment("WorkCenterPopover",
					"com.twobm.mobileworkorder.components.notificationCreate.fragments.WorkCenterFilterSelect",
					this);
				this._workCenterPopover.getAggregation('_dialog').getSubHeader().setVisible(false);
				// this._workCenterPopover.getAggregation('_dialog').getSubHeader().getContentMiddle()[0].setPlaceholder(this.getI18nText(
				// 	"NotificationCreate-WorkCenterFilterSelect-SearchField-Placeholder"));
				this._workCenterPopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
				this._workCenterPopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
				this._workCenterPopover.getAggregation('_dialog').setVerticalScrolling(true);

				this.setupWorkCenterSelectTemplate();

				this.getView().addDependent(this._workCenterPopover);
			}

			this._workCenterPopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
				path: "/WorkCenterSet",
				template: this.workCenterSelectTemplate,
				sorter: {
					path: 'Ktext',
					descending: false
				}
			});

			this._workCenterPopover.open();
		},
		setupWorkCenterSelectTemplate: function() {
			this.workCenterSelectTemplate = new sap.m.StandardListItem({
				title: "{Arbpl}",
				description: "{Ktext}",
				active: true
			});

			this.workCenterSelectTemplate.data("ID", "{Objid}");
			this.workCenterSelectTemplate.data("WORKCENTER", "{Arbpl}");
			this.workCenterSelectTemplate.data("KTEXT", "{Ktext}");
		},
		onFilterWorkCenterSelected: function(oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var ordModel = this.getView().getModel("ordModel");
			ordModel.getData().MnWkCtr = selectedItem.data("WORKCENTER");
			ordModel.refresh();
		},
		onSelectBtnPress: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			oRouter.navTo("structureBrowser", {
				parentView: "workOrderCreate"
			}, false);
		},

		onScanBtnPress: function() {
			var isHybridApp = this.getView().getModel("device").getData().isHybridApp;
			if (isHybridApp) {
				cordova.plugins.barcodeScanner.scan(
					function(result) {
						if (result.cancelled) {
							return;
						} else {
							var scannedEquipmentId = result.text;
							var onDataReceived = {
								success: function(oData, oResponse) {
									var isFuncLoc = false;
									this.saveEquipmentToModel(oData, isFuncLoc);

								}.bind(this),
								error: function(oData, oResponse) {
									if (oData.statusCode === "404") {
										this.searchFuncLocWithId(scannedEquipmentId);
									} else {
										this.errorCallBackShowInPopUp();
									}
								}.bind(this)
							};
							this.getView().getModel().read("/EquipmentsSet('" + scannedEquipmentId + "')", onDataReceived);
						}
					}.bind(this),
					function() {
						sap.m.MessageToast.show(this.getI18nText("MaterialScanningFailed"));
					}.bind(this), {
						showTorchButton: true, // iOS and Android
						//formats: "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
						disableAnimations: true, // iOS
						disableSuccessBeep: false // iOS
					}
				);
			} else {
				this.showAlertNotDevice();
			}
		},
		saveEquipmentToModel: function(equipmentObject, isFuncLoc) {

			if (isFuncLoc) {
				this.getView().getModel("ordModel").setProperty("/FunctLoc", equipmentObject.FunctionalLocation);
				this.getView().getModel("ordModel").setProperty("/FuncLocDesc", equipmentObject.Description);

			} else {
				this.getView().getModel("ordModel").setProperty("/FunctLoc", equipmentObject.Funcloc);
				this.getView().getModel("ordModel").setProperty("/FuncLocDesc", equipmentObject.Funclocdesc);
				this.getView().getModel("ordModel").setProperty("/Equipment", equipmentObject.Equipment);
				this.getView().getModel("ordModel").setProperty("/EquipmentDesc", equipmentObject.Description);
			}
		},
		searchFuncLocWithId: function(equipId) {
			var onDataReceived = {
				success: function(oData, oResponse) {
					var isFuncLoc = true;
					this.saveEquipmentToModel(oData, isFuncLoc);
				}.bind(this),
				error: function(oData, oResponse) {
					if (oData.statusCode === "404") {
						this.showAlertNoObjectFound(equipId);

					} else {
						this.errorCallBackShowInPopUp();
					}
				}.bind(this)
			};

			this.getView().getModel().read("/FunctionalLocationsSet('" + equipId + "')", onDataReceived);
		},
		showAlertNoObjectFound: function(equipmentNo) {
			// Reset values if object was not found
			this.getView().getModel("ordModel").setProperty("/FunctLoc", "");
			this.getView().getModel("ordModel").setProperty(this.newEntry.getPath() + "/FuncLocDesc", "");
			this.getView().getModel("ordModel").setProperty(this.newEntry.getPath() + "/Equipment", "");
			this.getView().getModel("ordModel").setProperty(this.newEntry.getPath() + "/EquipmentDesc", "");

			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.show(this.getI18nTextReplace1("Dashboard-scanObjectTile-searchObjectNotFoundMsgText", equipmentNo), {
				icon: MessageBox.Icon.NONE,
				title: this.getI18nText("Dashboard-scanObjectTile-searchObjectNotFoundMsgTitleText"),
				actions: [MessageBox.Action.OK],
				defaultAction: MessageBox.Action.OK,
				styleClass: bCompact ? "sapUiSizeCompact" : ""
			});
		},

		showAlertNotDevice: function() {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.show(this.getI18nText("Dashboard-scanObjectTile-isNotDeviceMsgText"), {
				icon: MessageBox.Icon.NONE,
				title: this.getI18nText("Dashboard-scanObjectTile-isNotDeviceMsgTitleText"),
				actions: [MessageBox.Action.OK],
				defaultAction: MessageBox.Action.OK,
				styleClass: bCompact ? "sapUiSizeCompact" : ""
			});
		},
		onNavigateBack: function(oEvent) {
			//Reset create notification model
			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");
			selectObjectForNewNotificationModel.setData({});
			this.getView().getModel("ordModel").setData({});
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			this.getView().setBindingContext(null);

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("dashboard", true);
			}
		},
		onSaveWorkOrder: function(oEvent) {

			var woModel = this.getView().getModel("ordModel").getData();
			var notifNo = woModel.NotifNo;
			var messageTitle = this.getView().getModel("i18n").getResourceBundle().getText("error");
			var message = this.getView().getModel("i18n").getResourceBundle().getText("pleaseFillInAllFields");
			if (notifNo === "NONOTIFICATIONID") {
				if (!woModel.OrderType || !woModel.ShortText || !woModel.MnWkCtr || !woModel.StartDate || !woModel.FinishDate || !woModel.FunctLoc) {
					sap.m.MessageBox.warning(message, {
						title: messageTitle
					});
					return;
				}
				notifNo = "";
			} else if (!woModel.OrderType || !woModel.ShortText || !woModel.MnWkCtr || !woModel.StartDate || !woModel.FinishDate) {
				sap.m.MessageBox.warning(message, {
					title: messageTitle
				});
				return;
			}

			var sDate = new Date(woModel.StartDate).toISOString().substr(0, 19);
			var fDate = new Date(woModel.FinishDate).toISOString().substr(0, 19);
			var myData = {
				"ShortText": woModel.ShortText,
				"NotifNo": notifNo,
				"OrderType": woModel.OrderType,
				"MnWkCtr": woModel.MnWkCtr,
				"StartDate": sDate,
				"FinishDate": fDate,
				"FunctLoc": woModel.FunctLoc,
				"Equipment": woModel.Equipment
			};
			this.getView().setBusy(true);
			
			var parameters = {
				success: function(myData) {
					this.getView().setBusy(false);
					MessageToast.show("Work Order Created successfully!");
					this.getView().getModel("ordModel").refresh();
					var ordId = "OrderSet('" + myData.Orderid + "')";
					this.getView().getModel("selectObjectForNewNotificationModel").setData({});
					this.getView().getModel("ordModel").setData({});
					this.getRouter().navTo("workOrderDetails", {
							workOrderContext: ordId
						}, true);
						
				}.bind(this),
				error: function(error) {
					this.getView().setBusy(false);
					var errorMessage = JSON.parse(error.responseText).error.message.value;
					var errorCode = JSON.parse(error.responseText).error.code;
					var messageTitle = this.getView().getModel("i18n").getResourceBundle().getText("error");
					sap.m.MessageBox.error("Error Message:" + errorMessage + "\n" + "\n Error Code:" + errorCode, {
						title: messageTitle
					});
					
				}.bind(this)
			};
			this.getView().getModel().create("/OrderSet", myData, parameters);
		}
	});
});