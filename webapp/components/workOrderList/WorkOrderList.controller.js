sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"com/twobm/mobileworkorder/components/offline/SyncStateHandler",
	"com/twobm/mobileworkorder/components/offline/SyncManager",
	'sap/m/MessageBox'
], function(Controller, History, SyncStateHandler, SyncManager, MessageBox) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.workOrderList.WorkOrderList", {
		mode: "MINE",
		firstLoad: true,

		onInit: function() {
			this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);

			this.getView().setModel(new sap.ui.model.json.JSONModel({
				mode: "MINE",
				filterAssigned: "ALL",
				filterWorkCenter: "ALL"
			}), "viewModel");
		},

		onRouteMatched: function(oEvent) {

			var sName = oEvent.getParameter("name");

			//Is it this page we have navigated to?
			if (sName !== "workOrderList") {
				return;
			}

			if (this.firstLoad) {
				this.firstLoad = false;
				this.filterData(true);
			}
		},

		onNavigationButtonPress: function(oEvent) {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = this.getRouter();
				oRouter.navTo("dashboard", {}, true);
			}
		},

		onWorkOrderItemPress: function(oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();

			if (this.mode === "MINE") {
				if (oEvent.getSource().getBindingContext().getObject().Assignedtome !== "X") {
					MessageBox.show(
						this.getI18nText("WorkOrderList-OpenUnsynchedReassignedOrderWarning"), {
							icon: MessageBox.Icon.INFORMATION,
							title: this.getI18nText("WorkOrderList-OpenUnsynchedReassignedOrderHeader"),
							actions: [MessageBox.Action.OK]
						}
					);
					return;
				}

				this.getRouter().navTo("workOrderDetails", {
					workOrderContext: oBindingContext.getPath().substr(1)
				});
			} else {
				this.getRouter().navTo("workOrderDetailsSummary", {
					workOrderContext: oBindingContext.getPath().substr(1)
				});
			}
		},

		// Pop-up for sorting and filter
		onViewSettingsDialogButtonPressed: function() {
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("com.twobm.mobileworkorder.components.workOrderList.fragments.OrderFilterDialog", this);
				this._oDialog.setModel(this.getView().getModel("i18n"), "i18n");
			}
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
			this._oDialog.open();
		},

		setInitialSorting: function() {
			var oTable = this.getView().byId("workOrderTableId");
			var oBinding = oTable.getBinding("items");

			var aSorters = [];

			var sortItem = "StartDate";
			var sortDescending = true;
			aSorters.push(new sap.ui.model.Sorter(sortItem, sortDescending));
			oBinding.sort(aSorters);
		},

		// Start filter and sorting based on selected.
		handleOrderFilterConfirm: function(oEvent) {
			var oView = this.getView();
			var oTable = oView.byId("workOrderTable");

			var mParams = oEvent.getParameters();
			var oBinding = oTable.getBinding("items");

			// apply sorter to binding
			// (grouping comes before sorting)
			var aSorters = [];
			// if (mParams.groupItem) {
			// 	var sPath = mParams.groupItem.getKey();
			// 	var bDescending = mParams.groupDescending;
			// 	var vGroup = this.orderGroupFunctions[sPath];
			// 	aSorters.push(new sap.ui.model.Sorter(sPath, bDescending, vGroup));
			// }

			var sortItem = mParams.sortItem.getKey();
			var sortDescending = mParams.sortDescending;
			aSorters.push(new sap.ui.model.Sorter(sortItem, sortDescending));
			oBinding.sort(aSorters);

			// apply filters to binding
			//var aFilters = [];
			//oBinding.filter(aFilters);

			var model = this.getView().getModel();
			model.refresh();
		},

		getOrderStatusIcon: function(orderStatus, personelNumber, assignedOperation) {
			if (orderStatus === "INITIAL")
				return "sap-icon://circle-task-2";

			if (orderStatus === "INPROGRESS")
				return "sap-icon://circle-task-2";

			return "sap-icon://circle-task-2";
		},

		getOrderStatusIconColor: function(orderStatus, personelNumber, assignedOperation) {
			if (orderStatus === "INITIAL") {
				return "#DBDBDB";
			} else if (orderStatus === "INPROGRESS") {
				return "#3AACF2";
			} else if (orderStatus === "COMPLETED") {
				return "#30D130";
			}
		},

		// orderFilterSelectPopUp: function(oEvent) {
		// 	var oButton = oEvent.getSource();

		// 	// create menu only once
		// 	if (!this._orderFilterMenu) {
		// 		this._orderFilterMenu = sap.ui.xmlfragment("com.twobm.mobileworkorder.components.workOrderList.fragments.OrderFilterSelect", this);
		// 		this.getView().addDependent(this._orderFilterMenu);
		// 	}

		// 	//var eDock = sap.ui.core.Popup.Dock;
		// 	this._orderFilterMenu.openBy(oButton); //.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);
		// },

		isInErrorState: function(woMatObjIsInErrorState) {
			if (woMatObjIsInErrorState === true) {
				return true;
			}
			return false;
		},

		// 
		isInErrorStateWorkOrder: function(errorsArray, orderId) {
			if ($.inArray(orderId, errorsArray) >= 0) {
				return true;
			} else {
				return false;
			}
		},

		isInErrorStateIconWorkOrder: function(errorsArray, orderId, assignedtome) {
			if ($.inArray(orderId, errorsArray) >= 0) {
				return "sap-icon://error";
			} else {
				if (this.mode === "MINE" && assignedtome !== "X")
					return "sap-icon://synchronize";
				else
					return "sap-icon://slim-arrow-right";
			}
		},

		isInErrorStateColorWorkOrder: function(errorsArray, orderId) {
			if ($.inArray(orderId, errorsArray) >= 0) {
				return "Red";
			} else {
				return "Gray";
			}
		},

		// isOnline: function() {
		// 	if (sap.hybrid) {
		// 		return false;
		// 	}

		// 	return true;
		// },

		onRefresh: function() {
			this.getView().getModel().refresh();
		},

		onFilterAssigned: function(oEvent) {
			var oButton = oEvent.getSource();

			// create action sheet only once
			if (!this._actionSheetAssigned) {
				this._actionSheetAssigned = sap.ui.xmlfragment(
					"com.twobm.mobileworkorder.components.workOrderList.fragments.FilterAssignedToActionSheet",
					this
				);
				this.getView().addDependent(this._actionSheetAssigned);
			}

			this._actionSheetAssigned.openBy(oButton);
		},

		onFilterAssignedSelect: function(oEvent) {
			if (!this._assignedPopover) {
				this._assignedPopover = sap.ui.xmlfragment("AssignedPopover",
					"com.twobm.mobileworkorder.components.workOrderList.fragments.FilterAssignedToPopover",
					this);

				this._assignedPopover.setModel(this.readingModel, "ViewModel");

				this.getView().addDependent(this._assignedPopover);
			}

			var filterAssigned = this.getView().getModel("viewModel").getProperty("/filterAssigned");

			if (filterAssigned !== "SELECTED") {
				var list = sap.ui.core.Fragment.byId("AssignedPopover", "reAssignEmployeeList");
				list.removeSelections(true);
				this.getView().getModel("viewModel").setProperty("/filterAssignedEmployees", []);
			}
			this._assignedPopover.open();
		},

		onFilterWorkCenter: function(oEvent) {
			var oButton = oEvent.getSource();

			// create action sheet only once
			if (!this._actionSheetWorkCenter) {
				this._actionSheetWorkCenter = sap.ui.xmlfragment(
					"com.twobm.mobileworkorder.components.workOrderList.fragments.FilterWorkCenterActionSheet",
					this
				);
				this.getView().addDependent(this._actionSheetWorkCenter);
			}

			this._actionSheetWorkCenter.openBy(oButton);
		},

		onFilterWorkCenterSelect: function(oEvent) {
			if (!this._workCenterPopover) {
				this._workCenterPopover = sap.ui.xmlfragment("WorkCenterPopover",
					"com.twobm.mobileworkorder.components.workOrderList.fragments.FilterWorkCenterPopover",
					this);

				this._workCenterPopover.setModel(this.readingModel, "ViewModel");

				this.getView().addDependent(this._workCenterPopover);
			}

			var filterWorkCenter = this.getView().getModel("viewModel").getProperty("/filterWorkCenter");

			if (filterWorkCenter !== "SELECTED") {
				var list = sap.ui.core.Fragment.byId("WorkCenterPopover", "reAssignEmployeeList");
				list.removeSelections(true);
				this.getView().getModel("viewModel").setProperty("/filterWorkCenters", []);
			}

			this._workCenterPopover.open();
		},

		onSelectSegment: function(selected) {
			this.mode = selected.getParameter("key");

			this.getView().getModel("viewModel").setProperty("/mode", selected.getParameter("key"));

			// Filter data
			this.filterData(this.mode === "MINE");
		},

		formatFilterVisible: function(mode) {
			return mode !== "MINE";
		},

		filterData: function(onlyMine) {
			var aFilters = [];

			if (onlyMine) {
				aFilters.push();

				var isHybridApp = this.getView().getModel("device").getData().isHybridApp;
				if (isHybridApp) {
					// In offline mode we would like to show orders that the user has assigned to them self, but have not yes synced to the backend
					aFilters.push(new sap.ui.model.Filter({
						filters: [
							new sap.ui.model.Filter("Assignedtome", sap.ui.model.FilterOperator.EQ, "X"),
							new sap.ui.model.Filter("Personresp", sap.ui.model.FilterOperator.EQ, this.getView().getModel("appInfoModel").getData().Persno)
						],
						and: false
					}));
				} else {
					//..but the above filter does not work online, so we need to have a different filter for online
					aFilters.push(new sap.ui.model.Filter("Assignedtome", sap.ui.model.FilterOperator.EQ, "X"));
				}

			} else {
				var filterAssigned = this.getView().getModel("viewModel").getProperty("/filterAssigned");
				var filterWorkCenter = this.getView().getModel("viewModel").getProperty("/filterWorkCenter");

				switch (filterAssigned) {
					case "ALL":
						break;
					case "UNASSIGNED":
						aFilters.push(new sap.ui.model.Filter("Personresp", sap.ui.model.FilterOperator.EQ, "00000000"));
						break;
					case "SELECTED":
						var employees = this.getView().getModel("viewModel").getProperty("/filterAssignedEmployees");
						var employeesFilters = [];

						for (var i = 0; i < employees.length; i++) {
							employeesFilters.push(new sap.ui.model.Filter("Personresp", sap.ui.model.FilterOperator.EQ, employees[i]));
						}

						aFilters.push(new sap.ui.model.Filter({
							filters: employeesFilters,
							and: false
						}));

						break;
				}

				switch (filterWorkCenter) {
					case "ALL":
						break;
					case "MINE":
						aFilters.push(new sap.ui.model.Filter("MnWkCtr", sap.ui.model.FilterOperator.EQ, this.getView().getModel("appInfoModel").getData()
							.WorkCenter));
						break;
					case "SELECTED":
						var workCenters = this.getView().getModel("viewModel").getProperty("/filterWorkCenters");
						var workCenterFilters = [];

						for (var y = 0; y < workCenters.length; y++) {
							workCenterFilters.push(new sap.ui.model.Filter("MnWkCtr", sap.ui.model.FilterOperator.EQ, workCenters[y]));
						}

						aFilters.push(new sap.ui.model.Filter({
							filters: workCenterFilters,
							and: false
						}));

						break;
				}
			}

			// update list binding
			var list = this.getView().byId("workOrderTable");
			var itemsBinding = list.getBinding("items");

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		formatEquipment: function(description, number) {
			if (number === "")
				return "";

			return number + " - " + description + "";
		},

		formatFunctionalLocation: function(description, number) {
			if (number === "")
				return "";

			return number + " - " + description + "";
		},

		onFilterAssignedAll: function() {
			this.getView().getModel("viewModel").setProperty("/filterAssigned", "ALL");
			this.filterData(false);
		},

		onFilterAssignedOnlyUnassigned: function() {
			this.getView().getModel("viewModel").setProperty("/filterAssigned", "UNASSIGNED");
			this.filterData(false);
		},

		onFilterAssignedSelected: function() {
			this.getView().getModel("viewModel").setProperty("/filterAssigned", "SELECTED");
			this.filterData(false);
		},

		onFilterWorkCenterAll: function() {
			this.getView().getModel("viewModel").setProperty("/filterWorkCenter", "ALL");
			this.filterData(false);
		},

		onFilterWorkCenterMine: function() {
			this.getView().getModel("viewModel").setProperty("/filterWorkCenter", "MINE");
			this.filterData(false);
		},

		onFilterWorkCenterSelected: function() {
			this.getView().getModel("viewModel").setProperty("/filterWorkCenter", "SELECTED");
			this.filterData(false);
		},

		formatAssignedTo: function(assignedTo, isPhone) {
			var labelText = "";

			if (!isPhone) {
				labelText = this.getI18nText("WorkOrderList-AssignedToFilterTextPrefix") + ": ";
			}
			switch (assignedTo) {
				case "ALL":
					labelText += this.getI18nText("WorkOrderList-AssignedToFilterAllText");
					break;
				case "UNASSIGNED":
					labelText += this.getI18nText("WorkOrderList-AssignedToFilterUnassignedText");
					break;
				case "SELECTED":
					var employees = this.getView().getModel("viewModel").getProperty("/filterAssignedEmployees");
					labelText += employees.length + " " + this.getI18nText("WorkOrderList-AssignedToFilterSelectedPostText");
					break;
			}

			return labelText;
		},

		formatWorkCenter: function(workCenter, isPhone) {
			var labelText = "";

			if (!isPhone) {
				labelText = this.getI18nText("WorkOrderList-WorkCenterFilterTextPrefix") + ": ";
			}
			switch (workCenter) {
				case "ALL":
					labelText += this.getI18nText("WorkOrderList-WorkCenterFilterAllText");
					break;
				case "MINE":
					labelText += this.getI18nText("WorkOrderList-WorkCenterFilterOnlyMineText");
					break;
				case "SELECTED":
					var workCenters = this.getView().getModel("viewModel").getProperty("/filterWorkCenters");
					labelText += workCenters.length + " " + this.getI18nText("WorkOrderList-WorkCenterFilterSelectedPostText");
					break;
			}

			return labelText;
		},

		onFilterWorkCenterOK: function(sender) {
			var list = sap.ui.core.Fragment.byId("WorkCenterPopover", "reAssignEmployeeList");

			if (list.getSelectedContextPaths().length < 1) {
				MessageBox.show(
					this.getI18nText("WorkOrderList-AssignedToFilterNoWorkCenterSelectedText"), {
						icon: MessageBox.Icon.INFORMATION,
						title: this.getI18nText("WorkOrderList-AssignedToFilterNoWorkCenterSelectedHeader"),
						actions: [MessageBox.Action.OK]
					}
				);
				return;
			}

			var workCenters = [];

			for (var i = 0; i < list.getSelectedContextPaths().length; i++) {
				var workCenter = this.getView().getModel().getData(list.getSelectedContextPaths()[i]).Arbpl;
				workCenters.push(workCenter);
			}

			this.getView().getModel("viewModel").setProperty("/filterWorkCenters", workCenters);
			this.getView().getModel("viewModel").setProperty("/filterWorkCenter", "ALL");
			this.getView().getModel("viewModel").setProperty("/filterWorkCenter", "SELECTED");
			this.filterData(false);

			this._workCenterPopover.close();
		},

		onFilterAssignedOK: function(sender) {
			var list = sap.ui.core.Fragment.byId("AssignedPopover", "reAssignEmployeeList");

			if (list.getSelectedContextPaths().length < 1) {
				MessageBox.show(
					this.getI18nText("WorkOrderList-AssignedToFilterNoUserSelectedText"), {
						icon: MessageBox.Icon.INFORMATION,
						title: this.getI18nText("WorkOrderList-AssignedToFilterNoUserSelectedHeader"),
						actions: [MessageBox.Action.OK]
					}
				);
				return;
			}

			var employees = [];

			for (var i = 0; i < list.getSelectedContextPaths().length; i++) {
				var employee = this.getView().getModel().getData(list.getSelectedContextPaths()[i]).Persno;
				employees.push(employee);
			}

			this.getView().getModel("viewModel").setProperty("/filterAssignedEmployees", employees);
			this.getView().getModel("viewModel").setProperty("/filterAssigned", "ALL");
			this.getView().getModel("viewModel").setProperty("/filterAssigned", "SELECTED");
			this.filterData(false);

			this._assignedPopover.close();
		},

		searchEmployeePress: function(oEvent) {
			var sValue = oEvent.getParameter("query");
			var searchString = sValue.toLowerCase();

			this.searchEmployee(searchString);
		},

		searchEmployeeLive: function(oEvent) {
			var sValue = oEvent.getParameter("newValue");
			var searchString = sValue.toLowerCase();

			this.searchEmployee(searchString);
		},

		searchEmployee: function(sValue) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();

			aFilters.push(new sap.ui.model.Filter("Searchstring", sap.ui.model.FilterOperator.Contains, searchString));

			// update list binding
			var list = sap.ui.core.Fragment.byId("AssignedPopover", "reAssignEmployeeList");
			var itemsBinding = list.getBinding("items");

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		searchWorkcenterPress: function(oEvent) {
			var sValue = oEvent.getParameter("query");
			var searchString = sValue.toLowerCase();

			this.searchWorkCenter(searchString);
		},

		searchWorkCenterLive: function(oEvent) {
			var sValue = oEvent.getParameter("newValue");
			var searchString = sValue.toLowerCase();

			this.searchWorkCenter(searchString);
		},

		searchWorkCenter: function(sValue) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();

			aFilters.push(new sap.ui.model.Filter("Searchstring", sap.ui.model.FilterOperator.Contains, searchString));

			// update list binding
			var list = sap.ui.core.Fragment.byId("WorkCenterPopover", "reAssignEmployeeList");
			var itemsBinding = list.getBinding("items");

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		onCloseAssignedPopover: function() {
			this._assignedPopover.close();
		},

		onCloseWorkCenterPopover: function() {
			this._workCenterPopover.close();
		},

		formatPersonresp: function(personresp) {
			if (Number(personresp) === 0) {
				return "";
			}

			return personresp;
		},

		formatAssignedToVisible: function(mode) {
			return mode !== "MINE";
		},
		
		formatCreateButtonVisible : function(mode){
			return mode === "MINE";
		},

		formatOrderTypeVisible: function(orderType) {
			return orderType === "PM02";
		},

		onCreateWorkOrder: function() {
			this.getRouter().navTo("workOrderCreate", {
				notificationId: "NONOTIFICATIONID" // nothing when entered from dashboard
			});
		}
	});
});