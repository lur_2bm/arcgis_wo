sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox",
	"sap/ui/model/Filter"
], function(Controller, History, MessageBox, Filter) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.notificationCreate.NotificationCreate", {
		onInit: function() {
			this.getRouter().getRoute("notificationCreate").attachMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function(oEvent) {
			var direction = sap.ui.core.routing.History.getInstance().getDirection();

			if (direction !== "Backwards") {

				this.getView().setModel(new sap.ui.model.json.JSONModel({
					WorkCenterSelectedText: "",
					WorkCenterSelectedID: "",
					NotificationTypeSelectedType: "",
					NotificationTypeSelectedText: "",
					NotificationPrioritySelectedType: "",
					NotificationPrioritySelectedText: ""
				}), "viewModel");

				this.newEntry = this.getView().getModel().createEntry("/NotificationsSet", {
					success: function(oData, oResponse) {
						this.getView().setBusy(false);
						this.getRouter().navTo("notificationDetails", {
							notificationContext: this.newEntry.getPath().substr(1)
						}, true);

					}.bind(this),
					error: function(error) {
						if (error.statusCode === 0) {
							return;
						}

						this.getView().setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				});
			}

			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");

			if (!jQuery.isEmptyObject(selectObjectForNewNotificationModel.getData())) {
				if (this.newEntry) {
					if (selectObjectForNewNotificationModel.getData().equipmentNo !== "NONE" && selectObjectForNewNotificationModel.getData().equipmentDesc !==
						"NONE") {
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", selectObjectForNewNotificationModel.getData().equipmentNo);
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/EquipmentDesc", selectObjectForNewNotificationModel.getData().equipmentDesc);
					}

					this.getView().getModel().setProperty(this.newEntry.getPath() + "/FuncLocDesc", selectObjectForNewNotificationModel.getData().funcLocDesc);
					this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctionalLoc", selectObjectForNewNotificationModel.getData().functionalLoc);
				}
			}

			this.getView().setBindingContext(this.newEntry);
		},

		onNavigateBack: function(oEvent) {
			//Reset create notification model
			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");
			selectObjectForNewNotificationModel.setData({});

			//Delete created entry
			this.getView().getModel().deleteCreatedEntry(this.getView().getBindingContext());

			//reset any error state
			this.getView().byId("shortDescription").setValueState("None");
			this.getView().byId("notificationTypeInput").setValueState("None");
			this.getView().byId("notificationPriorityInput").setValueState("None");

			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			this.getView().setBindingContext(null);

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("dashboard", {}, true);
			}
		},

		onSaveNotification: function() {
			if (this.notificationInformationValidates()) {

				this.getView().getModel().setProperty("NotifDate", new Date().toISOString().substr(0, 19), this.getView().getBindingContext());
				this.getView().getModel().setProperty("WorkCenterId", this.getView().getModel("viewModel").getData().WorkCenterSelectedID, this.getView()
					.getBindingContext());
				this.getView().getModel().setProperty("NotifType", this.getView().getModel("viewModel").getData().NotificationTypeSelectedType,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("Priority", this.getView().getModel("viewModel").getData().NotificationPrioritySelectedType,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("WorkCenterText", this.getView().getModel("viewModel").getData().WorkCenterSelectedText,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("NotifTypeText", this.getView().getModel("viewModel").getData().NotificationTypeSelectedText,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("PriorityText", this.getView().getModel("viewModel").getData().NotificationPrioritySelectedText,
					this.getView().getBindingContext());

				this.getView().setBusy(true);
				this.getView().getModel().submitChanges({
					success: jQuery.proxy(function(oData, oResponse) {
						this.getView().setBusy(false);
					}, this),
					error: jQuery.proxy(function(error) {
						this.getView().setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}, this)
				});
			} else {
				sap.m.MessageToast.show(this.getI18nText("NotificationCreate-SaveValidation-Failed"));
			}
		},

		notificationInformationValidates: function() {
			var validationError = false;

			//reset
			this.getView().byId("shortDescription").setValueState("None");
			this.getView().byId("notificationTypeInput").setValueState("None");
			this.getView().byId("notificationPriorityInput").setValueState("None");

			if (!this.getView().getBindingContext().getObject().ShortText) {
				this.getView().byId("shortDescription").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("notificationTypeInput").getValue()) {
				this.getView().byId("notificationTypeInput").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("notificationPriorityInput").getValue()) {
				this.getView().byId("notificationPriorityInput").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().getBindingContext().getObject().FunctionalLoc) {
				//this.getView().byId("funcLoc").setValueState("Error");
				validationError = true;
			}

			if (validationError) {
				return false;
			}

			return true;
		},

		onSelectBtnPress: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			oRouter.navTo("structureBrowser", {
				notificationContext: this.newEntry.getPath().substr(1),
				parentView: "notificationCreate"
			}, false);
		},

		onScanBtnPress: function() {
			var isHybridApp = this.getView().getModel("device").getData().isHybridApp;
			if (isHybridApp) {
				cordova.plugins.barcodeScanner.scan(
					function(result) {
						if (result.cancelled) {
							return;
						} else {
							var scannedEquipmentId = result.text;
							var onDataReceived = {
								success: function(oData, oResponse) {
									var isFuncLoc = false;
									this.saveEquipmentToModel(oData, isFuncLoc);

								}.bind(this),
								error: function(oData, oResponse) {
									if (oData.statusCode === "404") {
										this.searchFuncLocWithId(scannedEquipmentId);
									} else {
										this.errorCallBackShowInPopUp();
									}
								}.bind(this)
							};
							this.getView().getModel().read("/EquipmentsSet('" + scannedEquipmentId + "')", onDataReceived);
						}
					}.bind(this),
					function(error) {
						sap.m.MessageToast.show(this.getI18nText("MaterialScanningFailed"));
					}.bind(this), {
						showTorchButton: true, // iOS and Android
						torchOn: true, // Android, launch with the torch switched on (if available)
						orientation : "", // Android only (portrait|landscape), default unset so it rotates with the device
						formats: "ITF,CODE_128,EAN_8,EAN_13", // default: all but PDF_417 and RSS_EXPANDED
						disableAnimations: true, // iOS
						disableSuccessBeep: true // iOS
					}
				);
			} else {
				this.showAlertNotDevice();
			}
		},

		saveEquipmentToModel: function(equipmentObject, isFuncLoc) {

			if (isFuncLoc) {
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctionalLoc", equipmentObject.FunctionalLocation);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/FuncLocDesc", equipmentObject.Description);

			} else {
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctionalLoc", equipmentObject.Funcloc);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/FuncLocDesc", equipmentObject.Funclocdesc);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", equipmentObject.Equipment);
				this.getView().getModel().setProperty(this.newEntry.getPath() + "/EquipmentDesc", equipmentObject.Description);
			}
		},

		searchFuncLocWithId: function(equipId) {
			var onDataReceived = {
				success: function(oData, oResponse) {
					var isFuncLoc = true;
					this.saveEquipmentToModel(oData, isFuncLoc);
				}.bind(this),
				error: function(oData, oResponse) {
					if (oData.statusCode === "404") {
						this.showAlertNoObjectFound(equipId);

					} else {
						this.errorCallBackShowInPopUp();
					}
				}.bind(this)
			};

			this.getView().getModel().read("/FunctionalLocationsSet('" + equipId + "')", onDataReceived);
		},

		showAlertNoObjectFound: function(equipmentNo) {
			// Reset values if object was not found
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctionalLoc", "");
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/FuncLocDesc", "");
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", "");
			this.getView().getModel().setProperty(this.newEntry.getPath() + "/EquipmentDesc", "");

			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.show(this.getI18nTextReplace1("Dashboard-scanObjectTile-searchObjectNotFoundMsgText", equipmentNo), {
				icon: MessageBox.Icon.NONE,
				title: this.getI18nText("Dashboard-scanObjectTile-searchObjectNotFoundMsgTitleText"),
				actions: [MessageBox.Action.OK],
				defaultAction: MessageBox.Action.OK,
				styleClass: bCompact ? "sapUiSizeCompact" : ""
			});
		},

		showAlertNotDevice: function() {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.show(this.getI18nText("Dashboard-scanObjectTile-isNotDeviceMsgText"), {
				icon: MessageBox.Icon.NONE,
				title: this.getI18nText("Dashboard-scanObjectTile-isNotDeviceMsgTitleText"),
				actions: [MessageBox.Action.OK],
				defaultAction: MessageBox.Action.OK,
				styleClass: bCompact ? "sapUiSizeCompact" : ""
			});
		},

		onFilterWorkCenterSelect: function(oEvent) {
			if (!this._workCenterPopover) {
				this._workCenterPopover = sap.ui.xmlfragment("WorkCenterPopover",
					"com.twobm.mobileworkorder.components.notificationCreate.fragments.WorkCenterFilterSelect",
					this);
				this._workCenterPopover.getAggregation('_dialog').getSubHeader().setVisible(false);
				// this._workCenterPopover.getAggregation('_dialog').getSubHeader().getContentMiddle()[0].setPlaceholder(this.getI18nText(
				// 	"NotificationCreate-WorkCenterFilterSelect-SearchField-Placeholder"));
				this._workCenterPopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
				this._workCenterPopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
				this._workCenterPopover.getAggregation('_dialog').setVerticalScrolling(true);

				//this.setupWorkCenterSelectTemplate();

				this.getView().addDependent(this._workCenterPopover);
			}

			// this._workCenterPopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
			// 	path: "/WorkCenterSet",
			// 	template: this.workCenterSelectTemplate,
			// 	sorter: {
			// 		path: 'Ktext',
			// 		descending: false
			// 	}
			// });

			this._workCenterPopover.open();
		},

		// setupWorkCenterSelectTemplate: function() {
		// 	this.workCenterSelectTemplate = new sap.m.StandardListItem({
		// 		title: "{Arbpl}",
		// 		description: "{Ktext}",
		// 		active: true
		// 	});

		// 	this.workCenterSelectTemplate.data("ID", "{Objid}");
		// 	this.workCenterSelectTemplate.data("WORKCENTER", "{Arbpl}");
		// 	this.workCenterSelectTemplate.data("KTEXT", "{Ktext}");
		// },

		onFilterWorkCenterSelected: function(oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().WorkCenterSelectedType = selectedItem.getTitle();//.data("WORKCENTER");
			viewModel.getData().WorkCenterSelectedText = selectedItem.getDescription();//.data("KTEXT");
			viewModel.getData().WorkCenterSelectedID = selectedItem.data("ID");
			viewModel.refresh();
		},

		searchWorkCenterPress: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchWorkCenter(searchString, itemsBinding);
		},

		searchWorkCenterLive: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchWorkCenter(searchString, itemsBinding);
		},

		searchWorkCenter: function(sValue, itemsBinding) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();

			aFilters.push(new sap.ui.model.Filter("Ktext", sap.ui.model.FilterOperator.Contains, searchString));
			aFilters.push(new sap.ui.model.Filter("Ktext", sap.ui.model.FilterOperator.StartsWith, searchString));

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		formatSelectedWorkCenter: function(type, description) {
			if (type) {
				return type + " - " + description;
			}

			return "";
		},

		onFilterNotificationTypeSelect: function(oEvent) {
			if (!this._notificationTypePopover) {
				this._notificationTypePopover = sap.ui.xmlfragment("NotificationTypePopover",
					"com.twobm.mobileworkorder.components.notificationCreate.fragments.NotificationTypeFilterSelect",
					this);
				this._notificationTypePopover.getAggregation('_dialog').getSubHeader().setVisible(false);
				// this._notificationTypePopover.getAggregation('_dialog').getSubHeader().getContentMiddle()[0].setPlaceholder(this.getI18nText(
				// 	"NotificationCreate-NotificationTypeFilterSelect-SearchField-Placeholder"));
				this._notificationTypePopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
				this._notificationTypePopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
				this._notificationTypePopover.getAggregation('_dialog').setVerticalScrolling(true);

				// this.setupNotificationTypeSelectTemplate();

				this.getView().addDependent(this._notificationTypePopover);
			}

			// this._notificationTypePopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
			// 	path: "/NotificationTypesSet",
			// 	template: this.notificationTypeSelectTemplate,
			// 	sorter: {
			// 		path: "MessageTypeTxt",
			// 		descending: false
			// 	}
			// });

			this._notificationTypePopover.open();
		},

		// setupNotificationTypeSelectTemplate: function() {
		// 	this.notificationTypeSelectTemplate = new sap.m.StandardListItem({
		// 		title: "{MessageTypeTxt}",
		// 		description: "{MessageType}",
		// 		active: true
		// 	});

		// 	this.notificationTypeSelectTemplate.data("TYPE", "{MessageType}");
		// 	this.notificationTypeSelectTemplate.data("TEXT", "{MessageTypeTxt}");
		// },

		onFilterNotificationTypeSelected: function(oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().NotificationTypeSelectedType = selectedItem.getDescription();//data("TYPE");
			viewModel.getData().NotificationTypeSelectedText = selectedItem.getTitle();//data("TEXT");
			//Reset any previously selected priority
			viewModel.getData().NotificationPrioritySelectedType = "";
			viewModel.getData().NotificationPrioritySelectedText = "";
			viewModel.refresh();

			this.getView().byId("notificationTypeInput").setValueState("None");
		},

		searchNotificationTypePress: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchNotificationType(searchString, itemsBinding);
		},

		searchNotificationTypeLive: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchNotificationType(searchString, itemsBinding);
		},

		searchNotificationType: function(sValue, itemsBinding) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();

			aFilters.push(new sap.ui.model.Filter("MessageTypeTxt", sap.ui.model.FilterOperator.Contains, searchString));
			aFilters.push(new sap.ui.model.Filter("MessageTypeTxt", sap.ui.model.FilterOperator.StartsWith, searchString));

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		onFilterNotificationPrioritySelect: function(oEvent) {
			if (!this._notificationPriorityPopover) {
				this._notificationPriorityPopover = sap.ui.xmlfragment("NotificationPriorityPopover",
					"com.twobm.mobileworkorder.components.notificationCreate.fragments.NotificationPriorityFilterSelect",
					this);
				this._notificationPriorityPopover.getAggregation('_dialog').getSubHeader().setVisible(false);
				// this._notificationPriorityPopover.getAggregation('_dialog').getSubHeader().getContentMiddle()[0].setPlaceholder(this.getI18nText(
				// 	"NotificationCreate-NotificationPriotityFilterSelect-SearchField-Placeholder"));
				this._notificationPriorityPopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
				this._notificationPriorityPopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
				this._notificationPriorityPopover.getAggregation('_dialog').setVerticalScrolling(true);

				
				//this._notificationPriorityPopover.setModel(this.getView().getModel("viewModel"), "viewModel1");
				this.setupNotificationPrioritySelectTemplate();

				this.getView().addDependent(this._notificationPriorityPopover);
			}

			var viewModel = this.getView().getModel("viewModel");
			
			var aFilters = [];

			aFilters.push(new sap.ui.model.Filter("MessageType", sap.ui.model.FilterOperator.EQ, viewModel.getData().NotificationTypeSelectedType));

			this._notificationPriorityPopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
				path: "/NotificationPrioritiesSet",
				template: this.notificationPrioritySelectTemplate,
				filters: aFilters,
				sorter: {
					path: 'PriorityText',
					descending: false
				}
			});

			this._notificationPriorityPopover.open();
		},

		setupNotificationPrioritySelectTemplate: function() {
			this.notificationPrioritySelectTemplate = new sap.m.StandardListItem({
				title: "{PriorityText}",
				description : "{PriorityType}",
				active: true
			});

		// 	this.notificationPrioritySelectTemplate.data("TYPE", "{PriorityType}");
		// 	this.notificationPrioritySelectTemplate.data("TEXT", "{PriorityText}");
		 },

		onFilterNotificationPrioritySelected: function(oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().NotificationPrioritySelectedType = selectedItem.getDescription();//data("TYPE");
			viewModel.getData().NotificationPrioritySelectedText = selectedItem.getTitle();//.data("TEXT");
			viewModel.refresh();

			this.getView().byId("notificationPriorityInput").setValueState("None");
		},

		searchNotificationPriorityPress: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchNotificationPriority(searchString, itemsBinding);
		},

		searchNotificationPriorityLive: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var searchString = sValue.toLowerCase();
			var itemsBinding = oEvent.getParameter("itemsBinding");

			this.searchNotificationPriority(searchString, itemsBinding);
		},

		searchNotificationPriority: function(sValue, itemsBinding) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();

			aFilters.push(new sap.ui.model.Filter("PriorityText", sap.ui.model.FilterOperator.Contains, searchString));
			aFilters.push(new sap.ui.model.Filter("PriorityText", sap.ui.model.FilterOperator.StartsWith, searchString));

			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},

		formatNotificationTypeIsSelected: function(type) {
			if (type) {
				return true;
			}
			return false;
		},

		onShortDescriptionChanged: function() {
			//Reset any error state
			this.getView().byId("shortDescription").setValueState("None");
		}
	});
});