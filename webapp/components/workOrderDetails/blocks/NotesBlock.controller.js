sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/Fragment",
	"sap/m/MessageToast"
], function(Controller, Fragment, MessageToast) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.workOrderDetails.blocks.NotesBlock", {
		onInit: function() {
			//Subscribe to events
			this.getEventBus().subscribe("longTextDisplayMode", this.setLongTextInDisplayMode, this);
			this._showFormFragment("LongTextFragmentDisplay");

			this.createLongTextPopover();

			//setup edit note model
			var editNoteModel = this.getView().getModel("EditNoteModel");
			editNoteModel = new sap.ui.model.json.JSONModel();
			editNoteModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
			this.getView().setModel(editNoteModel, "EditNoteModel");

			this.clearEditNoteModel();
		},

		createLongTextPopover: function() {
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("LongTextPopover",
					"com.twobm.mobileworkorder.components.workOrderDetails.fragments.LongTextFragmentChangePopup",
					this);
				this._oPopover.attachBeforeClose(function() {});
				this.getView().addDependent(this._oPopover);
			}
		},

		openLongTextPopoverUpdate: function(oEvent) {
			var object = oEvent.getSource().getBindingContext().getObject();

			this._oPopover.setModel(new sap.ui.model.json.JSONModel(object));

			this._oPopover.open();
		},

		closeLongTextPopover: function() {
			if (this._oPopover) {
				this._oPopover.close();
			}
		},

		saveLongText: function(oEvent) {
			var orderNo = this.getView().getBindingContext().getObject().Orderid;

			var parameters = {
				success: function(oData, response) {
					this.closeLongTextPopover();
					this.getView().getModel().refresh();
				}.bind(this),
				error: function(error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			var dataUpdate = {
				LongText: this._oPopover.getModel().getData().LongText
			};

			var updatePath = this.getView().getBindingContext().getPath();//"/OrderSet(Orderid='" + orderNo + "')";

			this.getView().getModel().update(updatePath, dataUpdate, parameters);

		},

		clearEditNoteModel: function() {
			var editNoteModel = this.getView().getModel("EditNoteModel");

			var data = {
				Editable: false
			};

			editNoteModel.setData(data);
		},

		onExit: function() {
			for (var sPropertyName in this._formFragments) {
				if (!this._formFragments.hasOwnProperty(sPropertyName)) {
					return;
				}

				this._formFragments[sPropertyName].destroy();
				this._formFragments[sPropertyName] = null;
			}
		},

		_formFragments: {},

		_getFormFragment: function(sFragmentName) {
			var oFormFragment = this._formFragments[sFragmentName];

			if (oFormFragment) {
				return oFormFragment;
			}

			oFormFragment = sap.ui.xmlfragment(this.getView().getId(), "com.twobm.mobileworkorder.components.workOrderDetails.fragments." +
				sFragmentName, this);

			this._formFragments[sFragmentName] = oFormFragment;

			return oFormFragment;
		},

		_showFormFragment: function(sFragmentName) {
			var oPage = this.getView().byId("NotesLayoutId");

			oPage.removeAllContent();
			oPage.insertContent(this._getFormFragment(sFragmentName));
		},

		orderStatusValid: function(str) {
			var oContext = this.getView().getBindingContext();
			var model = this.getView().getModel();

			return !this.readOnly(oContext, model);
		},

		setLongTextInDisplayMode: function() {
			this._showFormFragment("LongTextFragmentDisplay");
		},

		getNotEditableNote: function(editable) {
			return !editable;
		}
	});
});