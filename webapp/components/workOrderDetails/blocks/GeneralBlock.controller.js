sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"com/twobm/mobileworkorder/util/Formatter"
], function(Controller, Formatter) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.workOrderDetails.blocks.GeneralBlock", {
		formatter: Formatter,
		
		formatPurchasingStatus: function(purchasingStatus) {
			switch (purchasingStatus) {
				case "PENDING":
					return this.getI18nText("WorkOrderDetails-GeneralBlock-PurchasingStatusPENDING");
			}

			return this.getI18nText("WorkOrderDetails-GeneralBlock-PurchasingStatusCOMPLETED");
		},

		navigateWithDirectionsToAddress: function(oEvent) {
			var street = oEvent.getSource().getBindingContext().getObject().Street;
			var city = oEvent.getSource().getBindingContext().getObject().City;

			var navigateurl = "https://www.google.com/maps/dir/?api=1&destination={0}&travelmode=driving"; //url used in online version

			if (window.cordova.require("cordova/platform").id === "ios") {
				navigateurl = "http://maps.apple.com/?daddr={0}&dirflg=d&t=h";
			}

			if (window.cordova.require("cordova/platform").id === "android") {
				navigateurl = navigateurl;
			}

			if (window.cordova.require("cordova/platform").id === "windows") {
				navigateurl = "bingmaps:?where={0}";

			}

			navigateurl = navigateurl.replace("{0}", encodeURI(street + ", " + city));

			if (this.getView().getModel("device").getData().isHybridApp) {
				window.location.assign(navigateurl);
				//window.open(navigateurl); //Will open the Attachment viewer
			} else {
				window.open(navigateurl);
			}
		},

		showOnlyIfDataAvailable: function(data) {
			if (data) {
				return true;
			}

			return false;
		},

		formatEquipment: function(equipment, description) {
			if (equipment) {
				return description + " (" + equipment + ")";
			}

			return "";
		},

		formatFuncloc: function(funcloc, description) {
			if (funcloc) {
				return description + " (" + funcloc + ")";
			}

			return "";
		},

		onNotificationPressed: function(oEvent) {
			var notifContext = "/NotificationsSet('" + oEvent.getSource().getBindingContext().getObject().NotifNo + "')";

			var data = {
				"block": "notification",
				"objectContext": notifContext.substr(1)
			};

			var eventBus = sap.ui.getCore().getEventBus();
			eventBus.publish("BlockNavigation", data);

		},

		isNotificationExist: function(NotifNo) {
			if (NotifNo !== "" && NotifNo !== undefined) {
				return true;
			} else {

				return false;
			}
		},
		
		formatAssignedTo : function(AssignedTo, Personresp){
			if(AssignedTo){
				return AssignedTo + " (" + Personresp + ")";
			}
			
			
		}
	});
});