sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.workOrderDetails.blocks.OperationsBlock", {
		onInit: function() {},

		gotoOperationDetailsPage: function(data) {
			var eventBus = sap.ui.getCore().getEventBus();
			eventBus.publish("BlockNavigation", data);
		},

		onOperationItemPress: function(oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();

			var data = {
				"block": "operation",
				"operationContext": oBindingContext.getPath().substr(1),
				"editable": this.getView().getModel("EditModeModel").getData().Editmode
			};

			this.gotoOperationDetailsPage(data);
		},

		onCompleOperationbtnPressed: function(oEvent) {
			var operationStatus = oEvent.getSource().getBindingContext().getObject().Complete;

			this.operationPath = oEvent.getSource().getBindingContext().getPath();

			var that = this;
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;

			if (operationStatus) {

				sap.m.MessageBox.show(this.getI18nText("WorkOrderDetails-OperationBlock-CancelCompletionAlertMsg"), {
					icon: sap.m.MessageBox.Icon.None,
					title: this.getI18nText("WorkOrderDetails-orderStatusTitle"),
					actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
					defaultAction: sap.m.MessageBox.Action.NO,
					styleClass: bCompact ? "sapUiSizeCompact" : "",
					onClose: function(oAction, object) {

						if (oAction === sap.m.MessageBox.Action.YES) {
							that.updateOperationStatus(that.operationPath, false);

						} else {
							return;
						}
					}
				});
			} else {
				this.updateOperationStatus(this.operationPath, true);
			}
		},

		updateOperationStatus: function(operationPath, complete) {
			var parameters = {
				success: function(oData, response) {
					this.getView().byId("idOperationTable").getBinding("items").refresh();
				}.bind(this),
				error: function(error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			var dataUpdate = {
				Complete: complete
			};

			this.getView().getModel().update(operationPath, dataUpdate, parameters);
		},

		opStatusColor: function(sStatus) {
			if (sStatus) {
				return "Transparent";
			} else {
				return "Transparent";
			}
		},

		opStatusIcon: function(sStatus) {
			if (sStatus) {
				return "sap-icon://accept";
			} else {
				return "sap-icon://circle-task";
			}
		},

		orderStatusValid: function(str) {
			var oContext = this.getView().getBindingContext();
			var model = this.getView().getModel();

			return !this.readOnly(oContext, model);
		},
		checkResponibleValid: function(responsible) {
			var responsibleIntValue = parseInt(responsible);
			if (responsibleIntValue !== 0) {
				return responsible;
			}
		},
		getFullname: function(fullname, username) {
			if (fullname) {
				return fullname + " (" + username + ")";
			}
		},
		addOperations: function(oEvent) {
			this.createOperationsPopover();
			this.getView().getModel("ViewModel").setProperty("/isEditing", "false");
			var path = this.getView().getBindingContext().getPath() + "/OrderOperation";
			this.getView().getModel().read(path, {
				success: function(result) {
					if (result.results.length > 0) {
						var opData = result.results.sort((a, b) => Number(b.Activity) - Number(a.Activity));
						var opActivity = parseInt(opData[0].Activity);
						opActivity = opActivity + 10;
						var oActivity;
						if (opActivity > 100) {
							oActivity = '0' + opActivity;
						} else {
							oActivity = '00' + opActivity;
						}
						this.newEntry = this.getView().getModel().createEntry(path, {
							properties: {
								//Orderid: opData[0].Orderid,
								Activity: oActivity.toString(),
								ControlKey: opData[0].ControlKey,
								WorkCntr: opData[0].WorkCntr,
								Plant: opData[0].Plant
							}
						});
						// Bind new item to popover
						this.operationPopover.setBindingContext(this.newEntry);
						// Open popover
						this.operationPopover.open();
					} else {
						//First operation - created offline

						this.newEntry = this.getView().getModel().createEntry(path, {
							properties: {
								//Orderid: opData[0].Orderid,
								Activity: '0010',
								ControlKey: this.getView().getBindingContext().getObject().OrderType, //opData[0].ControlKey,OrderType
								WorkCntr: this.getView().getBindingContext().getObject().MnWkCtr //opData[0].WorkCntr,
									//Plant: this.getView().getBindingContext().getObject().Maintplant, //opData[0].Plant
							}
						});
					}

					// Bind new item to popover
					this.operationPopover.setBindingContext(this.newEntry);
					// Open popover
					this.operationPopover.open();
				}.bind(this),
				error: function(error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
		},
		createOperationsPopover: function() {
			if (!this.operationPopover) {
				this.operationPopover = sap.ui.xmlfragment("OperationPopoverSelect",
					"com.twobm.mobileworkorder.components.workOrderDetails.fragments.AddOperationsView",
					this);
				this.getView().setModel(new sap.ui.model.json.JSONModel({
					isEditing: "false"
				}), "ViewModel");
				this.getView().addDependent(this.operationPopover);
			}
		},
		onSubmitOperations: function() {
			this.operationPopover.setBusy(true);
			if (this.getView().getModel().hasPendingChanges()) {
				var operationDesc = this.getView().getModel().getProperty(this.operationPopover.getBindingContext().getPath() + "/Description");
				var operationPersonr = this.getView().getModel().getProperty(this.operationPopover.getBindingContext().getPath() + "/Personresp");
				if (operationDesc !== "" && operationDesc !== undefined) {
					this.getView().getModel().submitChanges({
						success: function() {
							this.operationPopover.setBusy(false);
							if (!this.newEntry) {
								this.newEntry = null;
							}

							this.operationPopover.close();
						}.bind(this),
						error: function(error) {
							this.operationPopover.setBusy(false);
							self.errorCallBackShowInPopUp(error);
						}.bind(this)
					});
				} else {
					sap.m.MessageToast.show(this.getI18nText("WorkOrderDetails-OperationsBlock-Warning-SelectDescandPersonr"));
					this.operationPopover.setBusy(false);
					return;

				}
			} else {
				this.operationPopover.setBusy(false);
				this.operationPopover.close();
			}
		},
		handleValueHelpPersno: function() {
			if (!this.valueHelpPersonrDialog) {
				this.valueHelpPersonrDialog = sap.ui.xmlfragment(this.createId("OperationsPersonr"),
					"com.twobm.mobileworkorder.components.workOrderDetails.fragments.OperationPersonrValueHelp",
					this
				);
				// Attach to view
				this.getView().addDependent(this.valueHelpPersonrDialog)
			}
			// Show dialog				
			this.valueHelpPersonrDialog.open();

		},
		searchEmployeeLive: function(oEvent) {
			var sValue = oEvent.getParameters().value;
			var searchString = sValue.toLowerCase();
			this.searchEmployee(oEvent, searchString);
		},

		handleOperationPersonrSearch: function(oEvent) {
			var sValue = oEvent.getParameter("query");
			var searchString = sValue.toLowerCase();
			this.searchEmployee(oEvent, searchString);
		},
		searchEmployee: function(oEvent, sValue) {
			var aFilters = [];
			var searchString = sValue.toLowerCase();
			aFilters.push(new sap.ui.model.Filter("Searchstring", sap.ui.model.FilterOperator.Contains, searchString));

			// update list binding

			var itemsBinding = oEvent.getParameter("itemsBinding");
			if (itemsBinding) {
				itemsBinding.aApplicationFilters = [];

				if (aFilters.length > 0) {

					var filter = new sap.ui.model.Filter({
						filters: aFilters,
						and: true
					});

					itemsBinding.filter(filter);
				} else {
					itemsBinding.filter(aFilters);
				}
			}
		},
		handleValueHelpCloseCode: function(oEvent) {
			// Get selected item
			var selectedItem = oEvent.getParameter("selectedItem");
			if (selectedItem) {
				// Update values on model				
				this.getView().getModel().setProperty(this.operationPopover.getBindingContext().getPath() + "/Personresp", selectedItem.getInfo());
				this.getView().getModel().setProperty(this.operationPopover.getBindingContext().getPath() + "/Fullname", selectedItem.getTitle());
				this.getView().getModel().setProperty(this.operationPopover.getBindingContext().getPath() + "/Username", selectedItem.getDescription());
			}
		},
		onPopopoverClose: function(oEvent) {

			var isEditing = this.getView().getModel("ViewModel").getProperty("/isEditing");
			var pendingChanges = this.getView().getModel().hasPendingChanges();

			// reset changes in model if user close popOver and is in isEditing
			if (isEditing && pendingChanges) {
				var sPathArr = [];
				var sPath = oEvent.getSource().getBindingContext().getPath();
				sPathArr.push(sPath);
				this.getView().getModel().resetChanges(sPathArr);
			}
			// If we started creating a new entry..
			if (this.newEntry) {
				// ..make sure it is removed from the model if the user cancels
				this.getView().getModel().deleteCreatedEntry(this.newEntry);
				this.newEntry = null;
			}
			// Close the popup
			this.operationPopover.close();
		}
	});
});