sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.configuration.GeneralBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.GeneralBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.GeneralBlock",
                type: "XML"
            }
        }
    }
});