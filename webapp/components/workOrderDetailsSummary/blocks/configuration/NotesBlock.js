sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.configuration.NotesBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.NotesBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.NotesBlock",
                type: "XML"
            }
        }
    }
});