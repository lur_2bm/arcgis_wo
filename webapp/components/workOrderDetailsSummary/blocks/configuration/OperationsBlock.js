sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.configuration.OperationsBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.OperationsBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.workOrderDetailsSummary.blocks.OperationsBlock",
                type: "XML"
            }
        }
    }
});