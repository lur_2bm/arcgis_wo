sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/model/json/JSONModel",
	"com/twobm/mobileworkorder/util/Formatter",
	"sap/m/Label",
	"sap/m/MessageBox"

], function(Controller, JSONModel, iotModels, blackBirdmodels, Formatter, Label, MessageBox) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.equipmentDetails.blocks.GeneralBlock", {
		formatter: Formatter,
		formatFuncloc: function(funcloc, description) {
			if (funcloc) {
				return description + " (" + funcloc + ")";
			}

			return "";
		},

		formatParentLink: function(ParentEquipment, funcloc, description) {
			if (ParentEquipment) {
				return ParentEquipment;
			} else {
				return funcloc; //description + " (" + funcloc + ")";
			}

			return "";
		},

		onParentEquipmentPressed: function(oEvent) {
			var data;

			if (oEvent.getSource().getBindingContext().getObject().ParentEquipment) {
				//Equipment has parent equipment
				var equipObjectContext = "/EquipmentsSet('" + oEvent.getSource().getBindingContext().getObject().ParentEquipment + "')";

				data = {
					"block": "equipment",
					"objectContext": equipObjectContext.substr(1)
				};
			} else {
				//Equipment parent is func loc
				var objectContext = "/FunctionalLocationsSet('" + oEvent.getSource().getBindingContext().getObject().Funcloc + "')";
				data = {
					"block": "functionalLocation",
					"objectContext": objectContext.substr(1)
				};
			}

			sap.ui.getCore().getEventBus().publish("BlockNavigationEquipment", data);
		},

	

	});
});