sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"com/twobm/mobileworkorder/util/Formatter",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History"
], function(Controller, Formatter, JSONModel, History) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.equipmentDetails.blocks.EquipmentStructureListBlock", {

		onInit: function() {
			var equipModel = new JSONModel({
				items: ""
			});
			this.getView().setModel(equipModel, "equipModel");
			this.getView().attachModelContextChange(function(evt) {
				if (evt.getSource().getBindingContext().getObject() && this.currentContext !== evt.getSource().getBindingContext().getObject().Equipment) {
					this.currentContext = evt.getSource().getBindingContext().getObject().Equipment;
					this.getEquipmentByParent(evt.getSource().getBindingContext().getObject().Equipment);
				}
			}.bind(this));
		},
		getEquipmentByParent: function(equipNo) {
			this.viewModel = [];
			if (equipNo !== "" && equipNo !== undefined) {
				var filters = new Array();
				filters.push(new sap.ui.model.Filter("ParentEquipment", sap.ui.model.FilterOperator.Contains, equipNo));
				var sorters = new Array();
				sorters.push(new sap.ui.model.Sorter("Description", false, false));
				var url = "/EquipmentsSet";
				this.getView().setBusy(true);
				this.getView().getModel().read(url, {
					filters: filters,
					sorters: sorters,
					success: function(result) {
						var results = result.results;
						var children = [];
						for (var i = 0; i < results.length; i++) {
							var currentResult = results[i];
							children[children.length] = {
								name: currentResult.Equipment,
								description: currentResult.Description,
								id: currentResult.Equipment,
								parentId: currentResult.FuncLoc,
								parentEquipmentId: currentResult.ParentEquipment,
								leaf: currentResult.Isleaf,
								type: "EQUIPMENT",
								level: 0,
								uniqueId: this.createGUID(),
								parentFunctionalLocationName: currentResult.Funcloc,
								parentFunctionalLocationdescription: currentResult.Funclocdesc,
								icon:"sap-icon://technical-object"
							};
							if (equipNo === children[i].parentEquipmentId) {
								this.viewModel.splice(i, 0, children[i]);
							}
						}
						this.viewModel.sort(function(itemA, itemB) {
							if (itemA.description > itemB.description)
								return 1;
							else if (itemA.description < itemB.description)
								return -1;
							else return 0;
						});
						this.getView().getModel("equipModel").getData().Items = this.viewModel;
						this.getView().getModel("equipModel").refresh();
						this.getView().setBusy(false);
					}.bind(this),
					error: function(error) {
						this.getView().setBusy(false);
					}
				});
			}
		},
		createGUID: function() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random() * 16 | 0,
					v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		},
		onEquipmentPressed: function(oEvent) {
			var equipObjectContext = "/EquipmentsSet('" + this.getView().getModel("equipModel").getProperty(oEvent.getSource().getBindingContext(
				"equipModel").getPath()).name + "')";
			var eventBus = sap.ui.getCore().getEventBus();
			eventBus.publish("BlockNavigationEquipment", {
				"block": "equipment",
				"objectContext": equipObjectContext.substr(1)
			});
		}
	});

});