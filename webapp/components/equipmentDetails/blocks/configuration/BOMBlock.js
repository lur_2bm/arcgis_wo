sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.equipmentDetails.blocks.configuration.BOMBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.equipmentDetails.blocks.BOMBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.equipmentDetails.blocks.BOMBlock",
                type: "XML"
            }
        }
    }
});