sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.equipmentDetails.blocks.configuration.EquipmentStructureListBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.equipmentDetails.blocks.EquipmentStructureListBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.equipmentDetails.blocks.EquipmentStructureListBlock",
                type: "XML"
            }
        }
    }
});