sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"com/twobm/mobileworkorder/util/Formatter"
], function(Controller, History, Formatter) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.equipmentDetails.EquipmentDetails", {
		formatter: Formatter,

		onInit: function() {
			//this.getRouter("objectDetails").attachMatched(this.onRouteMatched, this);
			this.getRouter().getRoute("equipmentDetails").attachMatched(this.onRouteMatched, this);

			this.getEventBus().subscribe("BlockNavigationEquipment", this.performNavigationForBlocks, this);
		},

		performNavigationForBlocks: function(a, b, data) {

			if ('workOrder'.localeCompare(data.block) === 0) {
				this.getRouter().navTo("workOrderDetailsSummary", {
					workOrderContext: data.workOrderContext
				}, false);
			}
			if ('functionalLocation'.localeCompare(data.block) === 0) {
				this.getRouter().navTo("functionalLocationDetails", {
					objectContext: data.objectContext
				}, true);
			}
			if ('equipment'.localeCompare(data.block) === 0) {
				this.getRouter().navTo("equipmentDetails", {
					objectContext: data.objectContext
				}, true);
			}
		},

		onRouteMatched: function(oEvent) {
			var oArguments = oEvent.getParameter("arguments");
			var contextPath = '/' + oArguments.objectContext;
			var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

			//this.oContext is the current context of the view
			//this context is the context that was set when the view was shown the last time
			//therefore the new contextPath can be different from the contextPath/context
			//that was shown the last time the view was shown
			if (!this.getView().getBindingContext() || this.getView().getBindingContext().getPath() !== contextPath) {
				//Reset model to the new context
				this.ExpandLoaded = false;
				//this.oContext = givenContext;
				this.getView().setBindingContext(givenContext);
				this.getView().bindElement(contextPath);

				//	if (!this.getView().getBindingContext()) {
				this.scrollToTop();
				//	}
			}
			/*
			//do we have this context loaded in our model? We should always have a timeregistration entry
			if (this.ExpandLoaded) { //this.getView().getBindingContext().getObject()) {

				//if yes, refresh the model to reflect in memory model any changes done remotely to the order
				this.getView().getBindingContext().getModel().refresh(); //using true as argument got strange errors to arise

				//Update lists
				//Fix to get the lists to update after coming back to page with the same context


				//this.scrollToTop();
			} else {
				var that = this;
				//if not, create the binding context with all the expands we need in this view
				var aExpand = ["ObjectMeasurementPoint", "HistoricOrderSet", "HistoricNotifSet"];

				this.getView().getModel().createBindingContext(contextPath, "", {
						expand: aExpand.toString()
					},
					function(oEvent2) {

						that.ExpandLoaded = true;

					}, true);
			}*/
		},

		scrollToTop: function() {
			var generalSection = this.getView().byId("equipmentDetails-GeneralSubsection").getId();
			if (generalSection) {
				this.getView().byId("equipmentObjectPage").scrollToSection(generalSection, 0, -500);
			}
		},

		onNavigationButtonPress: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = this.getRouter();
				oRouter.navTo("dashboard", {}, true);
			}
		},

		onPressCreateNotification: function() {

			var oRouter = this.getRouter();

			//Getting object of currently selected object in this view
			var object = this.getView().getBindingContext().getObject();
			// creating a data to pass functional location no. and equipment no of current object.

			//var path = this.getView().getBindingContext().getPath().substr(1);

			//Reset create notification model
			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");
			selectObjectForNewNotificationModel.setData({});

			selectObjectForNewNotificationModel.getData().equipmentNo = object.Equipment;
			selectObjectForNewNotificationModel.getData().equipmentDesc = object.Description;
			selectObjectForNewNotificationModel.getData().functionalLoc = object.Funcloc;
			selectObjectForNewNotificationModel.getData().funcLocDesc = object.Funclocdesc;

			oRouter.navTo("notificationCreate", {
				// equipmentNo: object.Equipment,
				// equipmentDesc: object.Description,
				// functionalLoc: object.Funcloc,
				// funcLocDesc: object.Funclocdesc,
				// argAvailable: "true"
			}, false);
		},

		isParentEquipmentExist: function(parentEquip) {
			if (parentEquip) {
				return true;
			} else {
				return false;
			}
		},

		formatParentLink: function(ParentEquipment, funcloc, description) {
			if (ParentEquipment) {
				return ParentEquipment;
			} else {
				return funcloc; //description + " (" + funcloc + ")";
			}

			return "";
		},
		
		onParentEquipmentPressed: function(oEvent) {
			var data;

			if (oEvent.getSource().getBindingContext().getObject().ParentEquipment) {
				//Equipment has parent equipment
				var equipObjectContext = "/EquipmentsSet('" + oEvent.getSource().getBindingContext().getObject().ParentEquipment + "')";

				data = {
					"block": "equipment",
					"objectContext": equipObjectContext.substr(1)
				};
			} else {
				//Equipment parent is func loc
				var objectContext = "/FunctionalLocationsSet('" + oEvent.getSource().getBindingContext().getObject().Funcloc + "')";
				data = {
					"block": "functionalLocation",
					"objectContext": objectContext.substr(1)
				};
			}

			sap.ui.getCore().getEventBus().publish("BlockNavigationEquipment", data);
		}
	});
});