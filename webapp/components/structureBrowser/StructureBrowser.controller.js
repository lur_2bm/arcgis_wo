sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History"
], function(Controller, JSONModel, History) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.structureBrowser.StructureBrowser", {
		onInit: function() {
			this.currentSearchId = "";

			this.getView().setModel(new JSONModel({
				parentView: ""
			}), "visibleSelectButtonModel");

			this.getRouter().getRoute("structureBrowser").attachMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function(oEvent) {
			var direction = sap.ui.core.routing.History.getInstance().getDirection();

			if (direction !== "Backwards") {
				var oArguments = oEvent.getParameter("arguments");
				var model = this.getView().getModel("visibleSelectButtonModel");
				
				//Clear searchfield
				this.getView().byId("searchField").setValue("");

				// set the binding model for select button
				if ((oArguments.notificationContext && oArguments.parentView === "notificationCreate") || oArguments.parentView === "workOrderCreate") {
					model.setProperty("/parentView", oArguments.parentView);
				} else {
					model.setProperty("/parentView", "");
				}
				model.refresh();

				this.viewModel = [];

				this.getView().setModel(new JSONModel({
					items: this.viewModel,
					showEquipment: true
				}), "viewModel");

				this.getFunctionalLocationswithoutParewnt();
			}
		},

		onPressItem: function(oEvent) {
			var bindingContext = oEvent.getSource().getBindingContextPath();
			var item = this.getView().getModel("viewModel").getProperty(bindingContext);

			if (!item.expanded && !item.leaf) {
				item.expanded = true;

				if (item.parentId === "") {
					item.level = 0;
				}

				if (item.type === "FUNCTIONAL_LOCATION") {
					this.getFunctionalLocationsByParent(item);
				} else {
					this.getEquipmentByParentEquipment(item);
				}

			} else {
				this.closeItem(item);
			}

		},

		searchStructureLive: function(oEvent) {
			var sValue = oEvent.getParameter("newValue");
			var searchString = sValue.toLowerCase();

			this.searchStructure(searchString);
		},

		searchStructurePress: function(oEvent) {

			if (oEvent.getParameter("clearButtonPressed"))
				return;

			var sValue = oEvent.getParameter("query");
			var searchString = sValue.toLowerCase();
			
			this.getView().setBusy(true);

			this.searchStructure(searchString);
		},

		searchStructure: function(sValue) {

			if (sValue === "") {

				this.getFunctionalLocationswithoutParewnt();
				return;
			} else {
				var searchId = this.createGUID();
				this.currentSearchId = searchId;
			}

			var searchString = sValue.toLowerCase();
			//this.getView().setBusy(true);
			var filters = new Array();
			filters.push(new sap.ui.model.Filter("Searchstring", sap.ui.model.FilterOperator.Contains, searchString));
			//var filterByName = new sap.ui.model.Filter("ParentFuncLoc", sap.ui.model.FilterOperator.EQ, "");
			//filters.push(filterByName);
			var url = "/FunctionalLocationsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				success: function(result) {
					var results = result.results;
					var children = [];
					this.viewModel.length = 0;

					if (searchId !== this.currentSearchId) {
						return;
					}

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.FunctionalLocation,
							description: currentResult.Description,
							id: currentResult.FunctionalLocation,
							parentId: currentResult.ParentFuncLoc,
							leaf: currentResult.Isleaf,
							type: "FUNCTIONAL_LOCATION",
							level: 0,
							uniqueId: this.createGUID()
						};

						this.viewModel.splice(i, 0, children[i]);
					}

					this.searchEquipment(searchString, searchId);
				}.bind(this),
				error: function(error) {
					this.getView().setBusy(false);
				}
			});
		},

		searchEquipment: function(searchString, searchId) {

			var filters = new Array();
			filters.push(new sap.ui.model.Filter("Searchstring", sap.ui.model.FilterOperator.Contains, searchString));
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false,false));
			
			var url = "/EquipmentsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {

					if (searchId !== this.currentSearchId) {
						if (this.currentSearchId === "")
							this.getView().setBusy(false);

						return;
					}

					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.Equipment,
							description: currentResult.Description,
							id: currentResult.Equipment,
							parentId: currentResult.Funcloc,
							parentEquipmentId: currentResult.ParentEquipment,
							leaf: currentResult.Isleaf,
							type: "EQUIPMENT",
							level: 0,
							uniqueId: this.createGUID(),
							parentFunctionalLocationName: currentResult.Funcloc,
							parentFunctionalLocationdescription: currentResult.Funclocdesc
						};

						this.viewModel.splice(i, 0, children[i]);
					}

					this.viewModel.sort(function(itemA, itemB) {
						if (itemA.description > itemB.description)
							return 1;
						else if (itemA.description < itemB.description)
							return -1;
						else return 0;
					});

					this.getView().getModel("viewModel").refresh();
					this.getView().setBusy(false);
				}.bind(this),
				error: function(error) {
					this.getView().setBusy(false);
				}
			});
		},

		getFunctionalLocationswithoutParewnt: function() {
			//this.getView().setBusy(true);
			this.currentSearchId = "";
			this.viewModel.length = 0;

			var filters = new Array();
			var filterByName = new sap.ui.model.Filter("ParentFuncLoc", sap.ui.model.FilterOperator.EQ, "");
			filters.push(filterByName);
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false,false));
			
			var url = "/FunctionalLocationsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {
					var results = result.results;
					var children = [];

					if (this.currentSearchId !== "")
						return;

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.FunctionalLocation,
							description: currentResult.Description,
							id: currentResult.FunctionalLocation,
							parentId: currentResult.ParentFuncLoc,
							leaf: currentResult.Isleaf,
							type: "FUNCTIONAL_LOCATION",
							level: 0,
							uniqueId: this.createGUID()
						};

						this.viewModel.splice(i, 0, children[i]);
					}

					this.getView().getModel("viewModel").refresh();
					this.getView().setBusy(false);
				}.bind(this),
				error: function(error) {
					this.getView().setBusy(false);
				}
			});
		},

		getFunctionalLocationsByParent: function(item) {
			var itemIndex = this.viewModel.findIndex(function(currentItem) {
				return currentItem.uniqueId === item.uniqueId;
			});

			var filters = new Array();
			var filterByName = new sap.ui.model.Filter("ParentFuncLoc", sap.ui.model.FilterOperator.EQ, item.id);
			filters.push(filterByName);
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false, false));
			
			var url = "/FunctionalLocationsSet";
			item.busy = true;
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {
					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.FunctionalLocation,
							description: currentResult.Description,
							id: currentResult.FunctionalLocation,
							parentId: currentResult.ParentFuncLoc,
							uniqueParentId: item.uniqueId,
							leaf: currentResult.Isleaf,
							type: "FUNCTIONAL_LOCATION",
							level: item.level + 1,
							uniqueId: this.createGUID()
						};

						this.viewModel.splice(itemIndex + 1 + i, 0, children[i]);
					}

					this.getEquipmentByParentFunctionalLocation(item);
				}.bind(this),
				error: function(error) {
					item.busy = false;
				}
			});
		},

		getEquipmentByParentFunctionalLocation: function(item) {
			var itemIndex = this.viewModel.findIndex(function(currentItem) {
				return currentItem.id === item.id;
			});

			var filters = new Array();
			var filterByFLParent = new sap.ui.model.Filter("Funcloc", sap.ui.model.FilterOperator.EQ, item.id);
			filters.push(filterByFLParent);
			var filterByNoEQUParent = new sap.ui.model.Filter("ParentEquipment", sap.ui.model.FilterOperator.EQ, "");
			filters.push(filterByNoEQUParent);
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false, false));
			
			var url = "/EquipmentsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {
					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.Equipment,
							description: currentResult.Description,
							id: currentResult.Equipment,
							parentId: currentResult.Funcloc,
							uniqueParentId: item.uniqueId,
							parentEquipmentId: currentResult.ParentEquipment,
							leaf: currentResult.Isleaf,
							type: "EQUIPMENT",
							level: item.level + 1,
							uniqueId: this.createGUID(),
							parentFunctionalLocationName: currentResult.Funcloc,
							parentFunctionalLocationdescription: currentResult.Funclocdesc
						};

						this.viewModel.splice(itemIndex + 1 + i, 0, children[i]);
					}

					item.busy = false;
					this.getView().getModel("viewModel").refresh();
				}.bind(this),
				error: function(error) {
					item.busy = false;
				}
			});
		},

		getEquipmentByParentEquipment: function(item) {
			var itemIndex = this.viewModel.findIndex(function(currentItem) {
				return currentItem.uniqueId === item.uniqueId;
			});

			var filters = new Array();
			var filterByEQParent = new sap.ui.model.Filter("ParentEquipment", sap.ui.model.FilterOperator.EQ, item.id);
			filters.push(filterByEQParent);
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false, false));
			
			var url = "/EquipmentsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {
					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.Equipment,
							description: currentResult.Description,
							id: currentResult.Equipment,
							parentId: currentResult.Funcloc,
							uniqueParentId: item.uniqueId,
							parentEquipmentId: currentResult.ParentEquipment,
							leaf: currentResult.Isleaf,
							type: "EQUIPMENT",
							level: item.level + 1,
							uniqueId: this.createGUID(),
							parentFunctionalLocationName: currentResult.Funcloc,
							parentFunctionalLocationdescription: currentResult.Funclocdesc
						};

						this.viewModel.splice(itemIndex + 1 + i, 0, children[i]);
					}

					item.busy = false;
					this.getView().getModel("viewModel").refresh();
				}.bind(this),
				error: function(error) {
					item.busy = false;
				}
			});
		},

		closeItem: function(item) {
			var children = this.viewModel.filter(function(currentItem) {
				/*if (item.type === "FUNCTIONAL_LOCATION") {
					return currentItem.parentId === item.id || (currentItem.parentFunctionalLocationId === item.id && !currentItem.parentEquipmentId);
				} else {
					return currentItem.parentEquipmentId === item.id;
				}*/
				return currentItem.uniqueParentId === item.uniqueId;
			});
			for (var i = 0; i < children.length; i++) {
				this.closeItem(children[i]);
				var itemIndex = this.viewModel.findIndex(function(currentItem) {
					return currentItem.id === children[i].id;
				});
				this.viewModel.splice(itemIndex, 1);
				item.expanded = false;
			}

			this.getView().getModel("viewModel").refresh();
		},

		isLeaf: function(isLeaf, expanded) {
			if (expanded) {
				return "sap-icon://slim-arrow-down";
			} else if (!isLeaf) {
				return "sap-icon://slim-arrow-right";
			}

			return "";
		},

		typeIcon: function(type) {
			if (type === "FUNCTIONAL_LOCATION") {
				return "sap-icon://functional-location";
			}

			return "sap-icon://wrench";
		},

		navigateBack: function(oEvent) {
			window.history.go(-1);
		},

		onDetailsPress: function(oEvent) {
			if (this.getView().getModel("viewModel").getProperty(oEvent.getSource().getBindingContext("viewModel").getPath()).type ===
				"EQUIPMENT") {

				var objecContext = "/EquipmentsSet('" + this.getView().getModel("viewModel").getProperty(oEvent.getSource().getBindingContext(
					"viewModel").getPath()).name + "')";

				this.getRouter().navTo("equipmentDetails", {
					objectContext: objecContext.substr(1)

				}, false);
			} else {
				var funcObjectContext = "/FunctionalLocationsSet('" + this.getView().getModel("viewModel").getProperty(oEvent.getSource().getBindingContext(
					"viewModel").getPath()).name + "')";

				this.getRouter().navTo("functionalLocationDetails", {
					objectContext: funcObjectContext.substr(1)

				}, false);
			}
		},

		onSelectBtnPress: function(oEvent) {
			var object = this.getView().getModel("viewModel").getProperty(oEvent.getSource().getBindingContext(
				"viewModel").getPath());

			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");

			if (object.type !== "EQUIPMENT") {
				selectObjectForNewNotificationModel.getData().equipmentNo = "NONE";
				selectObjectForNewNotificationModel.getData().equipmentDesc = "NONE";
				selectObjectForNewNotificationModel.getData().functionalLoc = object.name;
				selectObjectForNewNotificationModel.getData().funcLocDesc = object.description;
			} else {
				selectObjectForNewNotificationModel.getData().equipmentNo = object.name;
				selectObjectForNewNotificationModel.getData().equipmentDesc = object.description;
				selectObjectForNewNotificationModel.getData().functionalLoc = object.parentFunctionalLocationName;
				selectObjectForNewNotificationModel.getData().funcLocDesc = object.parentFunctionalLocationdescription;
			}

			selectObjectForNewNotificationModel.refresh();

			this.navigateBack();
		},

		//formatter function to either show or hide the button
		EnableButtonVisbleCheck: function(str) {
			if (str === "notificationCreate" || str === "workOrderCreate") {
				return true;
			} else {
				return false;
			}
		},

		onSearch: function() {
			this.getRouter().navTo("structureSearch");
		},

		formatIndentWidth: function(level) {
			if (level === undefined)
				level = 0;

			return (level * 2) + 0.6 + "em";
		},

		createGUID: function() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random() * 16 | 0,
					v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		},

		onFilterEquipment: function(event) {
			var list = this.getView().byId("structureTable");
			var itemsBinding = list.getBinding("items");

			if (event.getSource().getPressed()) {

				this.getView().getModel("viewModel").setProperty("/showEquipment", false);

				var aFilters = [];
				aFilters.push(new sap.ui.model.Filter("type", sap.ui.model.FilterOperator.EQ, "FUNCTIONAL_LOCATION"));

				if (itemsBinding) {
					itemsBinding.aApplicationFilters = [];

					if (aFilters.length > 0) {

						var filter = new sap.ui.model.Filter({
							filters: aFilters,
							and: true
						});

						itemsBinding.filter(filter);
					} else {
						itemsBinding.filter(aFilters);
					}
				}

			} else {
				this.getView().getModel("viewModel").setProperty("/showEquipment", true);
				itemsBinding.filter();
			}
		},

		formatToggleEquipment: function(showEquipment) {
			if (showEquipment)
				return this.getI18nText("StructureBrowser-ButtonText-HideEquipment");
			else
				return this.getI18nText("StructureBrowser-ButtButtonTexton-EquipmentHidden");
		}
	});
});