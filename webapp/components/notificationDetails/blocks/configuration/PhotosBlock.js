sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.notificationDetails.blocks.configuration.PhotosBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.notificationDetails.blocks.PhotosBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.notificationDetails.blocks.PhotosBlock",
                type: "XML"
            }
        }
    }
});