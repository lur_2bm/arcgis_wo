sap.ui.define([
	"sap/m/MessageBox",
	"com/twobm/mobileworkorder/components/offline/SyncStateHandler"
], function(MessageBox, SyncStateHandler) {
	"use strict";
	return {
		needsSync: false,
		isSyncing: false,
		extraSyncNeeded: false,

		start: function(router) {
			// Attach online / offline events
			document.addEventListener("online", this.onConnected.bind(this), false);
			document.addEventListener("offline", this.onDisconnected.bind(this), false);
			// Attach to router so we can start sync when certain pages are shown
			router.attachRoutePatternMatched(this.onRouteMatched, this);

			this.registerForPush();
		},

		registerForPush: function() {
			sap.Push.registerForNotificationTypes(sap.Push.notificationType.badge | sap.Push.notificationType.sound | sap.Push.notificationType
				.alert,
				function(message) {

				}.bind(this),
				function(message) {
					// var pushregisterFailedText = sap.ui.getCore().getComponent(window.componentId).getModel("i18n").getResourceBundle().getText(
					// 	"SyncManager-PushRegisterFailed");
					// this.showMessage(pushregisterFailedText + ": " + message);
				}.bind(this),
				function(message) {
					this.sync(true);
				}.bind(this), "");
		},

		saveLastSyncTimeInBrowserCache: function(lastSyncTime) {
			jQuery.sap.require("jquery.sap.storage");

			if (jQuery.sap.storage.isSupported()) {
				//Get Storage object to use
				var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);

				// Set value in htlm5 storage 
				oStorage.put("LastSyncTime", lastSyncTime);
			}
		},

		onRouteMatched: function(oEvent) {
			// Get name of route
			var sName = oEvent.getParameter("name");

			// Are we navigating to a page that should trigger sync?
			if (sName === "dashboard" || sName === "workOrderList" || sName === "notificationList" || sName === "workOrdersFromWorkCenter") {
				// Update state that drives the sync UI
				SyncStateHandler.handleSyncState(false);
				// Sync
				this.syncIfNeeded(false);
			}
		},

		syncIfNeeded: function(refresh) {
			// Are there any pending changes?
			sap.hybrid.getOfflineStore().getRequestQueueStatus(
				function(status) {
					if (!status.isEmpty) {
						// Then sync
						this.sync(refresh);
					}
				}.bind(this));
		},

		sync: function(refresh) {
			// Only sync when there is a connection
			var syncStatusModel = sap.ui.getCore().getComponent(window.componentId).getModel("syncStatusModel");

			if (syncStatusModel.getData().Online) {

				if (this.isSyncing) {
					this.extraSyncNeeded = true;
					return;
				}

				this.isSyncing = true;

				// Show sync indicator
				this.setSyncIndicators(true);
				// Start sync
				sap.hybrid.synAppOfflineStore(function() {
						// Refresh default model to display any changes to data
						sap.ui.getCore().getComponent(window.componentId).getModel().refresh();

						var eventBus = sap.ui.getCore().getEventBus();
						eventBus.publish("OfflineStore", "Updated");

						// Set new date / time for last sync
						//var syncStatusModel = sap.ui.getCore().getComponent(window.componentId).getModel("syncStatusModel");
						var d = new Date();
						syncStatusModel.getData().LastSyncTime = d.toLocaleString();
						syncStatusModel.refresh();

						this.saveLastSyncTimeInBrowserCache(syncStatusModel.getData().LastSyncTime);

						this.isSyncing = false;

						if (this.extraSyncNeeded) {
							this.extraSyncNeeded = false;
							this.syncIfNeeded(false);
						}

						// Hide sync indicator
						this.setSyncIndicators(false);

						//Update sync state 
						SyncStateHandler.handleSyncState(true);
					}.bind(this),
					function(error) {

						var errorDueToLostNetworkConnection = error.includes("(The operation couldn’t be completed. Network is down)");

						//var syncStatusModel = sap.ui.getCore().getComponent(window.componentId).getModel("syncStatusModel");

						// Error during sync - most likely the device went offline during a sync
						if (syncStatusModel.getData().Online && !errorDueToLostNetworkConnection) {
							var text = sap.ui.getCore().getComponent(window.componentId).getModel("i18n").getResourceBundle().getText(
								"SyncManager-SyncError");
							// If this was not due to the device beeing offline, show the error
							this.showMessage(text + ": " + error);
						}

						this.isSyncing = false;

						// Hide sync indicator
						this.setSyncIndicators(false);
					}.bind(this), !refresh);
			}
		},

		showMessage: function(message) {
			var title = sap.ui.getCore().getComponent(window.componentId).getModel("i18n").getResourceBundle().getText(
				"SyncManager-MessageTitle");

			sap.m.MessageBox.show(message, {
				icon: sap.m.MessageBox.Icon.None,
				title: title,
				actions: [sap.m.MessageBox.Action.OK],
				defaultAction: sap.m.MessageBox.Action.NO,
				onClose: function(oAction, object) {}
			});
		},

		onDisconnected: function() {
			// Set connection status on sync model
			var syncStatusModel = sap.ui.getCore().getComponent(window.componentId).getModel("syncStatusModel");
			syncStatusModel.getData().Online = false;
			syncStatusModel.getData().IsSynching = false;
			syncStatusModel.getData().WentOfflineTime = new Date();
			// Update sync state
			SyncStateHandler.handleSyncState(false);
		},

		onConnected: function() {
			// Set connection status on sync model
			var syncStatusModel = sap.ui.getCore().getComponent(window.componentId).getModel("syncStatusModel");
			syncStatusModel.getData().Online = true;

			// Update sync state
			SyncStateHandler.handleSyncState(false);

			//Make sure that sync is not triggered if the internet connection is lost for a small period of time
			if (syncStatusModel.getData().WentOfflineTime) {
				var offlineTime = new Date(syncStatusModel.getData().WentOfflineTime);
				var now = new Date();
				var seconds = (now.getTime() - offlineTime.getTime()) / 1000;

				if (seconds > 15) {
					// Synchronize
					this.sync(true);
				}
			} else {
				this.sync(true);
			}

			this.registerForPush();

			syncStatusModel.getData().WentOfflineTime = null;
		},

		setSyncIndicators: function(isSynching) {
			var syncStatusModel = sap.ui.getCore().getComponent(window.componentId).getModel("syncStatusModel");
			syncStatusModel.getData().IsSynching = isSynching;
			syncStatusModel.refresh();
		}
	};
});