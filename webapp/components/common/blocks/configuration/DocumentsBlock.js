sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.common.blocks.configuration.DocumentsBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.common.blocks.DocumentsBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.common.blocks.DocumentsBlock",
                type: "XML"
            }
        }
    }
});