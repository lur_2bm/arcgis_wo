sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.common.blocks.configuration.PhotosBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.common.blocks.PhotosBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.common.blocks.PhotosBlock",
                type: "XML"
            }
        }
    }
});