sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.common.blocks.PhotosBlock", {

		onInit: function() {
			this.ImageModel = new sap.ui.model.json.JSONModel();
			this.createPhotoViewerPopover();
		},

		itemFactory: function(sId, oContext) {
			var image = new sap.m.Image();

			if (oContext.getObject()["@com.sap.vocabularies.Offline.v1.isLocal"]) {
				var path = oContext.getObject().__metadata.media_src;

				var request = {
					headers: {},
					requestUri: path,
					method: "GET"
				};

				OData.read(request,
					function(data, response) {
						var imageData = 'data:image/jpeg;base64,' + data;
						image.setSrc(imageData);
					},
					function(error) {
						var x = 0;
						//console.log("Error in read AffectedEntity");
					}
				);
			} else {
				image.setSrc(oContext.getObject().__metadata.media_src); //oContext.getProperty("__metadata.media_src");
			}

			//image.setSrc(imageData);
			image.setWidth("8em");
			image.setHeight("8em");
			image.setDensityAware(false);
			image.addStyleClass("sapUiSmallMarginEnd");
			image.addStyleClass("sapUiSmallMarginTop");
			image.attachPress(function(oEvent) {
				this.clickPreviewPhoto(oEvent);
			}.bind(this));

			return image;
		},

		onPhotoUpload: function(imageAsBase64) {
			var isHybridApp = this.getView().getModel("device").getData().isHybridApp;

			var date = new Date();
			var month = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
			var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
			var hour = (date.getHours() < 10 ? "0" : "") + date.getHours();
			var min = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
			var sec = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
			var dateStr = day + month + date.getFullYear() + "_" + hour + min + sec;
			var userName = this.getView().getModel("appInfoModel").getData().UserName;

			var fileName = "Photo_" + userName + "_" + dateStr + ".jpg";

			var fileData = {
				Filename: fileName,
				Changedate: new Date(),
				Createdby: userName
			};

			var parameters = {
				headers: {
					"slug": fileName,
					"Mimetype": 'image/jpeg'
				},
				success: function(data) {

					var parameters2 = {
						headers: {
							"slug": fileName,
							"if-match": data.__metadata.etag
						},
						success: function(dataUpdate) {
							var serviceUrl;
							var etag;
							if (data["@com.sap.vocabularies.Offline.v1.isLocal"]) {
								serviceUrl = data.__metadata.edit_media;
								etag = data.__metadata.media_etag;
							} else {
								serviceUrl = this.getView().getModel().sServiceUrl + "/PhotosSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
									"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')/$value";
							}

							//Upload the file stream to local db
							this.sendUploadRequest(imageAsBase64, fileName, serviceUrl, etag);
						}.bind(this),
						error: function(error) {
							this.errorCallBackShowInPopUp(error);
						}.bind(this)
					};

					//Update document entry
					var updatePath;
					if (data["@com.sap.vocabularies.Offline.v1.isLocal"]) {
						updatePath = data.__metadata.uri.replace(this.getView().getModel().sServiceUrl, "");
					} else {
						updatePath = "/PhotosSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
							"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')";
					}

					this.getView().getModel().update(updatePath, fileData, parameters2);

				}.bind(this),
				error: function(error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			//Create document entry
			var createPath = this.getView().getBindingContext().getPath() + "/PhotosSet";

			this.getView().getModel().create(createPath, fileData, parameters);
		},

		sendUploadRequest: function(imageData, fileName, serviceUrl, etag) {
			var xhr = new XMLHttpRequest();
			//var fileName = file.name;
			//var fileType = file.type;

			xhr.open("PUT", serviceUrl, false);
			xhr.setRequestHeader("Accept", "application/json");

			xhr.setRequestHeader("content-type", "text/plain"); // Not used by backend, but necessary for transport to backend
			xhr.setRequestHeader("x-csrf-token", this.getView().getModel().getSecurityToken());
			xhr.setRequestHeader("slug", fileName);
			xhr.setRequestHeader("if-match", etag);

			xhr.onreadystatechange = function() {
				if (xhr.readyState === 4) {
					if (xhr.status === 201) {
						var data = JSON.parse(xhr.response);
						if (data) {
							this.getView().byId("photosList").getBinding("items").refresh();
							console.log("Document created in offline db - " + "Src: " + data.d.__metadata.media_src);
						}
					} else if (xhr.status === 204) {
						this.getView().byId("photosList").getBinding("items").refresh();
						//var data = JSON.parse(xhr.response);
						// Note that no body for 204 response. 
						// Use data__metadata.media_src from previous read
						console.log("Media updated: ");

						//this.getView().byId("documentsList").getBinding("items").refresh();
					} else {
						console.log("Media create failed" + xhr.status + " " + xhr.response);
					}
				}
			}.bind(this);

			xhr.send(imageData);
		},

		// capturePhoto: function() {
		// 	var oNav = navigator.camera;
		// 	if (oNav) {
		// 		oNav.getPicture(function(file) {
		// 				var date = new Date();
		// 				var month = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
		// 				var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
		// 				var hour = (date.getHours() < 10 ? "0" : "") + date.getHours();
		// 				var min = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
		// 				var sec = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
		// 				var dateStr = day + month + date.getFullYear() + "_" + hour + min + sec;
		// 				var userName = this.getView().getModel("appInfoModel").getData().UserName;

		// 				var fileName = "Photo_" + userName + "_" + dateStr + ".jpg";

		// 				var fileData = {
		// 					//Orderid: this.getView().getBindingContext().getObject().Orderid,
		// 					//Data: file,
		// 					Createdate: new Date(),
		// 					Createdby: this.getView().getModel("appInfoModel").getData().UserName,
		// 					Filename: fileName
		// 				};

		// 				var parameters = {
		// 					headers: {
		// 						"slug": fileName,
		// 						"Mimetype": 'image/jpeg'
		// 					},
		// 					success: function(data) {

		// 						var parameters1 = {
		// 							headers: {
		// 								"slug": fileName,
		// 								"if-match": data.__metadata.etag
		// 							},
		// 							success: function(dataUpdate) {
		// 								var serviceUrl;
		// 								var etag;
		// 								if (data["@com.sap.vocabularies.Offline.v1.isLocal"]) {
		// 									serviceUrl = data.__metadata.edit_media;
		// 									etag = data.__metadata.media_etag;
		// 								} else {
		// 									serviceUrl = this.getView().getModel().sServiceUrl + "/PhotosSet(" + "Doctype='" + data.Doctype + "',Objky='" +
		// 										data.Objky +
		// 										"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')/$value";
		// 								}

		// 								//Upload the file stream to local db
		// 								this.sendUploadRequest(file, fileName, 'image/jpeg', serviceUrl, etag);
		// 							}.bind(this),
		// 							error: function(error) {
		// 								this.errorCallBackShowInPopUp(error);
		// 							}.bind(this)
		// 						};

		// 						//Update document entry
		// 						var updatePath;
		// 						if (data["@com.sap.vocabularies.Offline.v1.isLocal"]) {
		// 							updatePath = data.__metadata.uri.replace(this.getView().getModel().sServiceUrl, "");
		// 						} else {
		// 							updatePath = "/PhotosSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
		// 								"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')";
		// 						}

		// 						this.getView().getModel().update(updatePath, fileData, parameters1);

		// 					}.bind(this),
		// 					error: function(error) {
		// 						this.errorCallBackShowInPopUp(error);
		// 					}.bind(this)
		// 				};

		// 				var createPath = this.getView().getBindingContext().getPath() + "/PhotosSet";

		// 				this.getView().getModel().create(createPath, fileData, parameters);
		// 			}.bind(this),
		// 			function() { //Nothing happens when cancel photo input
		// 			}, {
		// 				quality: 20,
		// 				destinationType: oNav.DestinationType.DATA_URL, //Base64 - SaveToTemp: destinationType.FILE_URI
		// 				saveToPhotoAlbum: false
		// 			});
		// 	} else {
		// 		sap.m.MessageToast.show(this.getI18nText("Common-PhotosBlock-CameraNotAvailable"));
		// 	}
		// },

		capturePhoto: function() {
			var oNav = navigator.camera;
			if (oNav) {
				oNav.getPicture(this.onPhotoUpload.bind(this),
					function() { //Nothing happens when cancel photo input
					}, {
						quality: 20,
						destinationType: oNav.DestinationType.DATA_URL, //Base64 - SaveToTemp: destinationType.FILE_URI
						saveToPhotoAlbum: false,
						encodingType: oNav.EncodingType.JPEG,
						mediaType: oNav.MediaType.PICTURE
					});
			}
		},

		onDeletePhoto: function(oEvent) {
			var deletePath = this._oPopover.getModel("ImageModel").getData().ObjectUri;

			var messageBoxText = this.getI18nText("Common-PhotosBlock-DeletePhotoMessageText");

			sap.m.MessageBox.show(messageBoxText, {
				icon: sap.m.MessageBox.Icon.None,
				title: this.getI18nText("Common-PhotosBlock-DeletePhotoMessageHeader"),
				actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
				defaultAction: sap.m.MessageBox.Action.NO,
				styleClass: this.getView().$().closest(".sapUiSizeCompact").length ? "sapUiSizeCompact" : "",
				onClose: function(oAction, object) {

					if (oAction === sap.m.MessageBox.Action.YES) {
						var parameters = {
							success: function(data) {
								this.closePhotoPopupButton();
							}.bind(this),
							error: function(error) {
								this.errorCallBackShowInPopUp(error);
							}.bind(this)
						};

						this.getView().getModel().remove(deletePath, parameters);

					} else {
						return;
					}
				}.bind(this)
			});
			// var context = oEvent.getSource().getBindingContext();
			// var message = this.getI18nText("Common-PhotosBlock-DeletePhotoMessageText");
			// var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;

			// sap.m.MessageBox.show(message, {
			// 	icon: sap.m.MessageBox.Icon.None,
			// 	title: this.getI18nText("Common-PhotosBlock-DeletePhotoMessageHeader"),
			// 	actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
			// 	defaultAction: sap.m.MessageBox.Action.NO,
			// 	styleClass: bCompact ? "sapUiSizeCompact" : "",
			// 	onClose: function(oAction, object) {
			// 		if (oAction === sap.m.MessageBox.Action.YES) {

			// 			var deletePath;

			// 			if (this._oPopover.getModel("ImageModel").getData().LocalObjectUri) {
			// 				//Local db object
			// 				deletePath = this._oPopover.getModel("ImageModel").getData().LocalObjectUri;
			// 			} else {
			// 				var orderNo = this._oPopover.getModel("ImageModel").getData().OrderNo;
			// 				var attachmentId = this._oPopover.getModel("ImageModel").getData().AttachmentID;
			// 				// 
			// 				deletePath = "/AttachmentsSet(AttachmentID='" + attachmentId + "',Orderid='" + orderNo + "')";
			// 			}

			// 			var parameters = {
			// 				context: context,
			// 				eTag: "*",
			// 				success: function(oData, response) {
			// 					this.getView().byId("attachmentsList").getBinding("items").refresh(true);
			// 				}.bind(this),
			// 				error: function(error) {
			// 					this.errorCallBackShowInPopUp(error);
			// 				}.bind(this)

			// 			};

			// 			this.getView().getModel().remove(deletePath, parameters);

			// 			this._oPopover.close();
			// 		}
			// 	}.bind(this)
			// });
		},

		clickPreviewPhoto: function(oEvent) {
			var currentObject = oEvent.getSource().getBindingContext().getObject();

			this._oPopover.getModel("ImageModel").setData({
				ImageHeight: window.innerHeight - 300 + "px",
				ObjectUri: oEvent.getSource().getBindingContext().getPath()
			});

			if (currentObject["@com.sap.vocabularies.Offline.v1.isLocal"]) {
				var path = currentObject.__metadata.media_src;

				var request = {
					headers: {},
					requestUri: path,
					method: "GET"
				};

				OData.read(request,
					function(data, response) {
						var imageData = 'data:image/jpeg;base64,' + data;

						this._oPopover.getModel("ImageModel").getData().ImagePath = imageData;
						this._oPopover.getModel("ImageModel").refresh();
					}.bind(this),
					function(error) {
						var x = 0;
						//console.log("Error in read AffectedEntity");
					}
				);
			} else {
				this._oPopover.getModel("ImageModel").getData().ImagePath = currentObject.__metadata.media_src;
				this._oPopover.getModel("ImageModel").refresh();
			}

			this._oPopover.open();
		},

		createPhotoViewerPopover: function() {
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("com.twobm.mobileworkorder.components.common.fragments.PhotoViewerPopover",
					this);

				this._oPopover.setModel(this.ImageModel, "ImageModel");
				this._oPopover.attachBeforeClose(function() {});

				this.getView().addDependent(this._oPopover);
			}
		},

		closePhotoPopupButton: function() {
			if (this._oPopover) {
				this._oPopover.close();
			}
		},

		photoPopUpShowDeleteButton: function(attachmentID) {
			if (attachmentID === "") {
				return false;
			}
			return true;
		},

		orderStatusValid: function(str) {
			var oContext = this.getView().getBindingContext();
			var model = this.getView().getModel();

			return !this.readOnly(oContext, model);
		}
	});
});