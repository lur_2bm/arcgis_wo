sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller, JSONModel) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.common.blocks.DocumentsBlock", {

		downloadAllFiles: function(oEvent) {
			var downloadButtonAll = oEvent.getSource();
			var documentsPath = this.getView().byId("documentsList").getBindingContext().getPath();

			var syncStatusModel = this.getView().getModel("syncStatusModel");

			if (!syncStatusModel.getData().Online) {
				sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-DownloadNotPossible"));
				return;
			}

			//set buy cursor and disable documents list
			downloadButtonAll.setBusy(true);
			this.getView().byId("documentsList").setShowOverlay(true);

			var parameters = {

				success: function(oData, response) {
					var newStreamsRegistered = false;
					oData.results.some(function myFunction(item) {
						if (item['@com.sap.vocabularies.Offline.v1.mediaIsOffline'] || item.Isurl) {
							//Nothing
						} else {
							var path = "/DocumentsSet(Doctype='" + item.Doctype + "',Objky='" + item.Objky + "',Docnumber='" + item.Docnumber +
								"',Docpart='" + item.Docpart + "',Docversion='" + item.Docversion + "')";

							newStreamsRegistered = true;

							sap.hybrid.getOfflineStore().registerStreamRequest(item.Objky + item.Docnumber, path,
								function() {}.bind(this),
								function(error) {
									//Stream has probably already been registered for offline handling
								});
						}
					});

					if (newStreamsRegistered) {
						//Call store.refresh
						sap.hybrid.getOfflineStore().refresh(function(data) {
								this.getView().byId("documentsList").setShowOverlay(false);
								this.getView().byId("documentsList").getBinding("items").refresh(true);
								downloadButtonAll.setBusy(false);
								sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-AllFilesDownloaded"));
							}.bind(this),
							function(error) {
								downloadButtonAll.setBusy(false);
								this.getView().byId("documentsList").getBinding("items").refresh(true);
								this.getView().byId("documentsList").setShowOverlay(false);
								console.log("Failed to download stream");
							}.bind(this));
					} else {
						downloadButtonAll.setBusy(false);
						this.getView().byId("documentsList").setShowOverlay(false);
						sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-AllFilesDownloaded"));
					}

				}.bind(this),
				error: function(error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			this.getView().getModel().read(documentsPath + "/DocumentsSet", parameters);
		},

		downloadDocument: function(oEvent) {
			var downloadButton = oEvent.getSource();
			var currentObject = oEvent.getSource().getBindingContext().getObject();
			var fileUrl = this.getView().getModel().sServiceUrl + oEvent.getSource().getBindingContext().getPath() + "/$value";

			if (this.getView().getModel("device").getData().isHybridApp) {
				var syncStatusModel = this.getView().getModel("syncStatusModel");

				if (!syncStatusModel.getData().Online) {
					sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-DownloadNotPossible"));
					return;
				}

				var streamID = currentObject.Objky + currentObject.Docnumber;

				sap.hybrid.getOfflineStore().registerStreamRequest(streamID, oEvent.getSource().getBindingContext()
					.getPath(),
					function() {
						downloadButton.setBusy(true);
						sap.hybrid.getOfflineStore().refresh(function(data) {
								downloadButton.setBusy(false);
								this.getView().byId("documentsList").getBinding("items").refresh(true);
							}.bind(this),
							function(error) {
								alert("Failed to download stream: " + error.Message);
							}.bind(this), [streamID]);
					}.bind(this),
					function(error) {
						//Stream has probably already been registered for offline handling
						this.getView().byId("documentsList").getBinding("items").refresh(true);
					}.bind(this));
			} else {
				// Online in browser - just open the file link and the file will be downloaded by browser
				window.open(fileUrl);
			}
		},
		viewDocument: function(oEvent) {
			//This method is only called if the solution is running in Hybrid mode. This is controlled by view formatter
			var currentObject = oEvent.getSource().getBindingContext().getObject();

			//If currentObject is only a URL link just open in window.open
			if (currentObject.Isurl) {
				window.open(currentObject.Filename);
				return;
			}

			var platformName = window.cordova.require("cordova/platform").id;
			var directoryUrl;
			if (platformName === "ios") {
				directoryUrl = cordova.file.dataDirectory; // + "TempFiles/";
			} else if (platformName === "windows") {
				directoryUrl = cordova.file.dataDirectory;
			}
			
			var uri = encodeURI(currentObject.__metadata.media_src);

			var fullpath = directoryUrl + encodeURI(currentObject.Filename);
			fullpath = fullpath.replace(new RegExp("%20", 'g'), ""); //Replace spaces or else the file cannot open locally
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
				fs.root.getFile(currentObject.Filename, {
					create: true,
					exclusive: false
				}, function(fileEntry) {
					var oReq = new XMLHttpRequest();
					// Make sure you add the domain name to the Content-Security-Policy <meta> element.
					oReq.open("GET", uri, true);
					// Define how you want the XHR data to come back
					oReq.responseType = "blob";
					oReq.onload = function(oEvent) {
						var blob = oReq.response; // Note: not oReq.responseText
						if (blob) {
							// Create a URL based on the blob, and set an <img> tag's src to it.
							var url = window.URL.createObjectURL(blob);
							window.open(url);
							// Or read the data with a FileReader
							// var reader = new FileReader();
							// reader.addEventListener("loadend", function() {
							// 	// reader.result contains the contents of blob as text
							// });
							// reader.readAsText(blob);
						} else {
							sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-OpenDocumentFailed"));
							console.error('we didnt get an XHR response!');
						}
					};
					oReq.send(null);
				}, function(err) {
					sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-OpenDocumentFailed"));
					console.error('error getting file! ' + err);
				});
			}, function(err) {
				sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-OpenDocumentFailed"));
				console.error('error getting persistent fs! ' + err);
			});
		},

		onFileSelected: function(oEvent) {
			var isHybridApp = this.getView().getModel("device").getData().isHybridApp;
			var contentType = oEvent.getParameters().files[0].type;
			var file = oEvent.getParameters().files[0];
			var fileName = file.name;
			var fileType = file.type;

			var userFullName = this.getView().getModel("appInfoModel").getData().UserFullName;
			var userName = this.getView().getModel("appInfoModel").getData().UserName;

			if (isHybridApp && window.cordova.require("cordova/platform").id === "ios") {
				var re = /(?:\.([^.]+))?$/;
				var fileExtension = re.exec(fileName)[1];

				if (fileExtension && fileExtension.toLowerCase() === "mov") {
					var date = new Date();
					var month = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
					var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
					var hour = (date.getHours() < 10 ? "0" : "") + date.getHours();
					var min = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
					var sec = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
					var dateStr = day + month + date.getFullYear() + "_" + hour + min + sec;

					fileName = "Video_" + userName + "_" + dateStr + "." + fileExtension;
				}
			}

			var fileData = {
				Filename: fileName,
				Changedate: new Date(),
				Fullname: userFullName,
				Createdby: userName,
				Chnageby: userName,
				Mimetype: fileType
			};

			var parameters = {
				headers: {
					"slug": fileName,
					"Mimetype": contentType
				},
				success: function(data) {

					var parameters2 = {
						headers: {
							"slug": fileName,
							"if-match": data.__metadata.etag
						},
						success: function(dataUpdate) {
							var serviceUrl;
							var etag;
							if (data["@com.sap.vocabularies.Offline.v1.isLocal"]) {
								serviceUrl = data.__metadata.edit_media;
								etag = data.__metadata.media_etag;
							} else {
								serviceUrl = this.getView().getModel().sServiceUrl + "/DocumentsSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
									"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')/$value";
							}

							//Upload the file stream to local db
							this.sendUploadRequest(file, serviceUrl, etag);
						}.bind(this),
						error: function(error) {
							this.errorCallBackShowInPopUp(error);
						}.bind(this)
					};

					//Update document entry
					var updatePath;
					if (data["@com.sap.vocabularies.Offline.v1.isLocal"]) {
						updatePath = data.__metadata.uri.replace(this.getView().getModel().sServiceUrl, "");
					} else {
						updatePath = "/DocumentsSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
							"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')";
					}

					this.getView().getModel().update(updatePath, fileData, parameters2);

				}.bind(this),
				error: function(error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			//Create document entry
			var createPath = this.getView().getBindingContext().getPath() + "/DocumentsSet";

			this.getView().getModel().create(createPath, fileData, parameters);
		},
		//Before upload started
		onUploadStarted: function(oControlEvent) {
			sap.ui.core.BusyIndicator.show();
		},

		onUploadComplete: function(oControlEvent) {
			sap.ui.core.BusyIndicator.hide();
			var oFileUploader = this.getView().byId("customFileUploader");
			oFileUploader.destroyHeaderParameters();

			this.getView().byId("documentsList").getBinding("items").refresh(true);
		},

		sendUploadRequest: function(file, serviceUrl, etag) {
			var xhr = new XMLHttpRequest();
			var fileName = file.name;
			var fileType = file.type;

			xhr.open("PUT", serviceUrl, false);
			xhr.setRequestHeader("Accept", "application/json");

			xhr.setRequestHeader("content-type", fileType);
			xhr.setRequestHeader("x-csrf-token", this.getView().getModel().getSecurityToken());
			xhr.setRequestHeader("slug", fileName);
			xhr.setRequestHeader("if-match", etag);

			xhr.onreadystatechange = function() {
				if (xhr.readyState === 4) {
					if (xhr.status === 201) {
						var data = JSON.parse(xhr.response);
						if (data) {
							this.getView().byId("documentsList").getBinding("items").refresh(true);
							console.log("Document created in offline db - " + "Src: " + data.d.__metadata.media_src);
						}
					} else if (xhr.status === 204) {
						//var data = JSON.parse(xhr.response);
						// Note that no body for 204 response. 
						// Use data__metadata.media_src from previous read
						console.log("Media updated: ");

						//this.getView().byId("documentsList").getBinding("items").refresh();
					} else {
						console.log("Media create failed" + xhr.status + " " + xhr.response);
					}
				}
			}.bind(this);

			xhr.send(file);
		},

		deleteDocument: function(oEvent) {
			var currentObject = oEvent.getSource().getBindingContext().getObject();
			var deletePath = oEvent.getSource().getBindingContext().getPath();

			var messageBoxText = this.getI18nText("Common-DocumentsBlock-DeleteDocumentValidationMessage") + " " + currentObject.Filename;

			sap.m.MessageBox.show(messageBoxText, {
				icon: sap.m.MessageBox.Icon.None,
				title: this.getI18nText("Common-DocumentsBlock-DeleteDocumentValidationHeader"),
				actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
				defaultAction: sap.m.MessageBox.Action.NO,
				styleClass: this.getView().$().closest(".sapUiSizeCompact").length ? "sapUiSizeCompact" : "",
				onClose: function(oAction, object) {

					if (oAction === sap.m.MessageBox.Action.YES) {
						var parameters = {
							success: function(data) {

							}.bind(this),
							error: function(error) {
								this.errorCallBackShowInPopUp(error);
							}.bind(this)
						};

						this.getView().getModel().remove(deletePath, parameters);

					} else {
						return;
					}
				}.bind(this)
			});
		},

		showDownloadButton: function(mediaIsOffline, isHybridApp, isUrl) {
			if (isUrl) {
				return false;
			}

			if (isHybridApp) {
				if (mediaIsOffline) {
					return false;
				} else {
					return true;
				}
			} else {
				//If online solution always show download button
				return true;
			}
		},

		showViewButton: function(mediaIsOffline, isHybridApp, isUrl) {
			if (isUrl) {
				return true;
			}

			if (isHybridApp) {
				if (mediaIsOffline) {
					return true;
				} else {
					return false;
				}
			} else {
				//If online solution never show view button
				return false;
			}
		},

		getDocumentIcon: function(mimetype) {
			switch (mimetype) {
				case "video/quicktime":
					return "sap-icon://attachment-video";
				case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
				case "application/msword":
					return "sap-icon://doc-attachment";
				case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
				case "application/msexcel":
					return "sap-icon://excel-attachment";
				case "image/png":
				case "image/jpg":
				case "image/jpeg":
					return "sap-icon://picture";
				case "application/pdf":
					return "pdf-attachment";
				case "plain/text":
					return "attachment-text-file";

				default:
					return "sap-icon://document";
			}
		},
		showDeleteButton: function(loggedInUserName, createdBy) {
			if (loggedInUserName === createdBy) {
				return true;
			}
			return false;

		}
	});
});