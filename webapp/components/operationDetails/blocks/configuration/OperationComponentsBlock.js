sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.operationDetails.blocks.configuration.OperationComponentsBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.operationDetails.blocks.OperationComponentsBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.operationDetails.blocks.OperationComponentsBlock",
                type: "XML"
            }
        }
    }
});