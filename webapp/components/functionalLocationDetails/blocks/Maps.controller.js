sap.ui.define([
	// 	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/mvc/Controller",
	"com/twobm/mobileworkorder/util/Maps"
], function(Controller, Maps) {
	Controller.extend('com.twobm.mobileworkorder.components.functionalLocationDetails.blocks.Maps', {
		initializeMap(baseMapName, mapDivId, centerPoint, zoomLevel) {
			Maps.require([
				'esri/Map',
				'esri/views/MapView',
				'dojo/domReady!'
			], function(Map, MapView) {
				MapView({
					map: Map({
						basemap: baseMapName,
					}),
					container: mapDivId,
					center: centerPoint,
					zoom: zoomLevel
				});
			});
		},

		onInit() {
			// 			this.initializeMap('streets', 'mapContainer', [8.641874, 49.293589], 8);
			this.initializeMap('streets', 'mapContainer', [12.600556, 55.681946], 8);
		}
	});
});