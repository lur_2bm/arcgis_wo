sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.components.functionalLocationDetails.blocks.configuration.FuncLocStructureListBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.components.functionalLocationDetails.blocks.FuncLocStructureListBlock",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.components.functionalLocationDetails.blocks.FuncLocStructureListBlock",
                type: "XML"
            }
        }
    }
});