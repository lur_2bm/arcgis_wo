sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"com/twobm/mobileworkorder/util/Formatter",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History"
], function(Controller, Formatter, JSONModel, History) {
	"use strict";

	return Controller.extend("com.twobm.mobileworkorder.components.functionalLocationDetails.blocks.FuncLocStructureListBlock", {

		onInit: function() {

			var funcModel = new JSONModel({
				items: ""
			});
			this.getView().setModel(funcModel, "funcModel");
			this.getView().attachModelContextChange(function(evt) {
				if (evt.getSource().getBindingContext().getObject() && this.currentContext !== evt.getSource().getBindingContext().getObject().FunctionalLocation) {
					this.currentContext = evt.getSource().getBindingContext().getObject().FunctionalLocation;
					this.getFunctionLocationsByParent(evt.getSource().getBindingContext().getObject().FunctionalLocation);
				}
			}.bind(this));
		},

		getFunctionLocationsByParent: function(FunLoc) {
			this.viewModel = [];
			if (FunLoc !== "" && FunLoc !== undefined) {
				this.viewModel.length = 0;
				var filters = new Array();
				filters.push(new sap.ui.model.Filter("FunctionalLocation", sap.ui.model.FilterOperator.Contains, FunLoc));
				var url = "/FunctionalLocationsSet";
				this.getView().setBusy(true);
				this.getView().getModel().read(url, {
					filters: filters,
					success: function(result) {
						var results = result.results;
						var children = [];
						for (var i = 0; i < results.length; i++) {
							var currentResult = results[i];
							children[children.length] = {
								name: currentResult.FunctionalLocation,
								description: currentResult.Description,
								id: currentResult.FunctionalLocation,
								parentId: currentResult.ParentFuncLoc,
								leaf: currentResult.Isleaf,
								type: "FUNCTIONAL_LOCATION",
								level: 0,
								uniqueId: this.createGUID(),
								icon: "sap-icon://functional-location"
							};
							if (FunLoc === currentResult.ParentFuncLoc) {
								this.viewModel.splice(i, 0, children[i]);
							}
						}
						this.getView().getModel("funcModel").getData().Items = this.viewModel;
						this.getView().getModel("funcModel").refresh();
						this.getEquipmentByParentFunctionalLocation(FunLoc);
						this.getView().setBusy(false);
					}.bind(this),
					error: function(error) {
						this.getView().setBusy(false);
					}
				});
			}
		},
		createGUID: function() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random() * 16 | 0,
					v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		},
		onFunctionLocationPressed: function(oEvent) {
			var eventBus;
			var oType = this.getView().getModel("funcModel").getProperty(oEvent.getSource().getBindingContext(
				"funcModel").getPath()).type;
			var oName = this.getView().getModel("funcModel").getProperty(oEvent.getSource().getBindingContext(
				"funcModel").getPath()).name;
			if(oType === "FUNCTIONAL_LOCATION") {		
			var funcObjectContext = "/FunctionalLocationsSet('" + oName + "')";
			var eventBus = sap.ui.getCore().getEventBus();
			eventBus.publish("BlockNavigationFunctionalLocation", {
				"block": "functionalLocation",
				"objectContext": funcObjectContext.substr(1)
			});
			}
			if(oType === "EQUIPMENT"){
			var equipObjectContext = "/EquipmentsSet('" + oName + "')";
			eventBus = sap.ui.getCore().getEventBus();
			eventBus.publish("BlockNavigationFunctionalLocation", {
				"block": "equipment",
				"objectContext": equipObjectContext.substr(1)
			});	
			}
		},
		getEquipmentByParentFunctionalLocation: function(item) {
			var itemIndex = this.getView().getModel("funcModel").getData().Items.length;
			var filters = new Array();
			var filterByFLParent = new sap.ui.model.Filter("Funcloc", sap.ui.model.FilterOperator.EQ, item);
			filters.push(filterByFLParent);
			var filterByNoEQUParent = new sap.ui.model.Filter("ParentEquipment", sap.ui.model.FilterOperator.EQ, "");
			filters.push(filterByNoEQUParent);
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false, false));
			
			var url = "/EquipmentsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {
					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.Equipment,
							description: currentResult.Description,
							id: currentResult.Equipment,
							parentId: currentResult.Funcloc,
							uniqueParentId: item.uniqueId,
							parentEquipmentId: currentResult.ParentEquipment,
							leaf: currentResult.Isleaf,
							type: "EQUIPMENT",
							level: item.level + 1,
							uniqueId: this.createGUID(),
							parentFunctionalLocationName: currentResult.Funcloc,
							parentFunctionalLocationdescription: currentResult.Funclocdesc,
							icon: "sap-icon://technical-object"
						};

						this.viewModel.splice(itemIndex + 1 + i, 0, children[i]);
					}
					this.getView().setBusy(false);
					this.getView().getModel("funcModel").getData().Items = this.viewModel;
					this.getView().getModel("funcModel").refresh();
				}.bind(this),
				error: function(error) {
					this.getView().setBusy(false);
				}
			});
		}

	});

});